#include "TimeSheet.h"

namespace lab3
{
	//constructor
	TimeSheet::TimeSheet(const char* name, unsigned int maxEntries)
		: mStr(name), mMax(static_cast<int>(maxEntries))
	{
		mP = new int[mMax];
		mCnt = 0;

		//Initializing inside of mP
		for (int i = 0; i < mMax; ++i)
		{
			mP[i] = 0;
		}
	}
	
	//copy constructor
	TimeSheet::TimeSheet(const TimeSheet& ts) 
		: mStr(ts.mStr), mMax(ts.mMax)
	{
		mP = new int[mMax];
		mCnt = ts.mCnt;

		for (int i = 0; i < mMax; ++i)
		{
			mP[i] = 0;
		}

		Mymemcpy(mP, ts.mP, mCnt);
	}

	TimeSheet& TimeSheet::operator=(const TimeSheet& p)
	{
		if (this != &p) 
		{
			delete[] mP;

			mStr = p.mStr;
			mMax = p.mMax;

			mP = new int[mMax];
			mCnt = p.mCnt;

		}
		else
		{
			return *this;
		}

		for (int i = 0; i < mMax; ++i)
		{
			mP[i] = 0;
		}

		Mymemcpy(mP, p.mP, mCnt);
		return *this;
	}

	//destructor
	TimeSheet::~TimeSheet()
	{
		delete[] mP;
	}

	void TimeSheet::AddTime(int timeInHours)
	{
		//<LOGIC>
		//mMax만큼 timeInHours를 넣어줄수 있게 설정
		//timeInHours의 limit 설정

		//1시간 ~ 10시간 사이의 timeInHours만 삽입
		if (1 <= timeInHours && timeInHours <= 10)
		{
			if (mMax == mCnt || mMax < mCnt)
			{
				return;
			}

			mP[mCnt] = timeInHours;
			++mCnt;
		}
		else
		{
			return;
		}
		
	}

	int TimeSheet::GetTimeEntry(unsigned int index) const
	{
		int getTimeEntryIndex = static_cast<int>(index);

		if (getTimeEntryIndex < 0 || mCnt <= getTimeEntryIndex || mP[index] == 0)
		{
			return -1;
		}
		else 
		{
			return mP[index];
		}
	}

	int TimeSheet::GetTotalTime() const
	{
		int totalTime = 0;

		for (int i = 0; i < mMax; ++i)
		{
			totalTime = totalTime + mP[i];
		}
		return totalTime;
	}

	float TimeSheet::GetAverageTime() const
	{
		if (mCnt == 0)
		{
			return 0.0f;
		}

		return GetTotalTime() / static_cast<float>(mCnt);
	}

	float TimeSheet::GetStandardDeviation() const
	{
		if (mCnt == 0)
		{
			return 0.0f;
		}

		float mean = GetAverageTime();
		float result = 0.0;

		for (int i = 0; i < mCnt; ++i)
		{
			result += static_cast<float>(pow(mP[i] - mean, 2));
		}

		return static_cast<float>(sqrt(result / mCnt));
	}

	const std::string& TimeSheet::GetName() const
	{
		return mStr;
	}


}