#pragma once
#include <string>
#include <math.h>
#include "MyIostream.h"

using namespace std;

namespace lab3
{
	class TimeSheet
	{
	public:
		TimeSheet(const char* name, unsigned int maxEntries);
		TimeSheet(const TimeSheet& ts);
		~TimeSheet();
		void AddTime(int timeInHours);
		int GetTimeEntry(unsigned int index) const;
		int GetTotalTime() const;
		float GetAverageTime() const;
		float GetStandardDeviation() const;
		const std::string& GetName() const;
		TimeSheet& operator=(const TimeSheet& p);

	private:
		string mStr;
		int mLen;
		int mMax;
		int* mP;
		int mCnt;
		
		// 필요에 따라 private 변수와 메서드를 추가하세요.
	};
}