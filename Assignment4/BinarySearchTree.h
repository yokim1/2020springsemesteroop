#pragma once

#include <memory>
#include <vector>
#include <stack>
#include <queue>

using namespace std;

namespace assignment4
{
	template<typename T>
	class TreeNode;

	template<typename T>
	class BinarySearchTree final
	{
	public:
		void Insert(std::unique_ptr<T> data);
		void Insert(std::shared_ptr<TreeNode<T>> parentNode, std::unique_ptr<T> data);
		bool Search(const T& data);

		bool Delete(const T& data);
		void changeParent(std::shared_ptr<TreeNode<T>> deleteNode, std::shared_ptr<TreeNode<T>> changeNode);
		
		/*shared_ptr<TreeNode<T>> minValueNode(std::shared_ptr<TreeNode<T>> node);
		shared_ptr<TreeNode<T>> Delete(std::shared_ptr<TreeNode<T>> parentNode ,const T& data);
		*/

		const std::weak_ptr<TreeNode<T>> GetRootNode() const;

		static std::vector<T> TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode);

	private:
		shared_ptr<TreeNode<T>> mRoot;
	};

	template<typename T>
	void BinarySearchTree<T>::Insert(std::unique_ptr<T> data)
	{
		if (mRoot == nullptr)
		{
			mRoot = make_shared<TreeNode<T>>(move(data));
		}
		else
		{
			Insert(mRoot, std::move(data));
		}
	}

	template<typename T>
	void BinarySearchTree<T>::Insert(std::shared_ptr<TreeNode<T>> parentNode, std::unique_ptr<T> data)
	{
		if (*data <= *parentNode->Data)
		{
			if (parentNode->Left == nullptr)
			{
				parentNode->Left = std::make_shared<TreeNode<T>>(std::move(data));
				parentNode->Left->Parent = parentNode;
			}
			else
			{
				Insert(parentNode->Left, std::move(data));
			}
		}
		else if (*parentNode->Data < *data)
		{
			if (parentNode->Right == nullptr)
			{
				parentNode->Right = std::make_shared<TreeNode<T>>(std::move(data));
				parentNode->Right->Parent = parentNode;
			}
			else
			{
				Insert(parentNode->Right, std::move(data));
			}
		}
	}

	template<typename T>
	const std::weak_ptr<TreeNode<T>> BinarySearchTree<T>::GetRootNode() const
	{
		//std::shared_ptr<TreeNode<T>> n;
		//return n;

		return mRoot;
	}

	template<typename T>
	bool BinarySearchTree<T>::Search(const T& data)
	{
		std::shared_ptr<TreeNode<T>>ptr = mRoot;

		while (ptr != nullptr)
		{
			if (data == *ptr->Data)
			{
				return true;
			}

			if (data <= *ptr->Data)
			{
				ptr = ptr->Left;
			}
			else if (*ptr->Data < data)
			{
				ptr = ptr->Right;
			}
		}
		return false;
	}

	template<typename T>
	bool BinarySearchTree<T>::Delete(const T& data)
	{
		//logic
		//1. tree에 data가 존재하는지 찾는다.
		//2. 있다면, 해당 node까지 ptr을 이동 시킨다.
		//3. 해당 ptr에서 3가지를 고려한다.
		//3-1. children node들이 없는 경우
		//3-2. children node들이 2개가 있는 경우
		//3-3. children node가 1개가 있는 경우
		
		//1.tree에 data가 존재하는지 찾는다.
		if (this->Search(data) == false || mRoot == nullptr)
		{
			return false;
		}


		//2.있다면, 해당 node까지 ptr을 이동 시킨다.
		shared_ptr<TreeNode<T>> ptr = mRoot;

		while (true)
		{
			if (data < *ptr->Data)
			{
				ptr = ptr->Left;
			}
			else if (data > *ptr->Data)
			{
				ptr = ptr->Right;
			}
			else if (data == *ptr->Data)
			{
				break;
			}
		}
		

		//3.
		//3-1. children node들이 없는 경우
		if ((ptr->Left == nullptr) && (ptr->Right == nullptr))
		{
			if (ptr == mRoot)
			{
				mRoot = nullptr;
				return true;
			}
			
			shared_ptr<TreeNode<T>> parent = ptr->Parent.lock();

			if (parent->Left == ptr)
			{
				parent->Left = nullptr;
			}
			else if (parent->Right = ptr)
			{
				parent->Right = nullptr;
			}
			
			return true;
		}
		
		//3-2. children node들이 2개가 있는 경우
		else if ((ptr->Left != nullptr) && (ptr->Right != nullptr))
		{
			//후계자를 찾으러 간다.
			shared_ptr<TreeNode<T>> successor = ptr->Right;
			shared_ptr<TreeNode<T>> parent = ptr->Parent.lock();

			while (successor->Left != nullptr)
			{
				successor = successor->Left;
			}

			//Tree에서 successor를 때어낸다.
				if (successor->Parent.lock()->Left == successor)
				{
					successor->Parent.lock()->Left = nullptr;
				}
				else if (successor->Parent.lock()->Right == successor)
				{
					successor->Parent.lock()->Right = nullptr;
				}

			//delete할 대상을 successor로 replace한다.
			if (ptr == mRoot)
			{
				mRoot = successor;
			}
			else if (parent->Left == ptr)
			{
				parent->Left = successor;
			}
			else if (parent->Right == ptr)
			{
				parent->Right = successor;
			}
			successor->Parent = ptr->Parent;

			successor->Left = ptr->Left;
			successor->Right = ptr->Right;

			if (successor->Left != nullptr)
			{
				successor->Left->Parent = successor;
			}
			if (successor->Right != nullptr)
			{
				successor->Right->Parent = successor;
			}

			return true;
		}

		//3-3. children node가 1개가 있는 경우
		else if (ptr->Left != nullptr)
		{
			shared_ptr<TreeNode<T>> parent = ptr->Parent.lock();
			shared_ptr<TreeNode<T>> child = ptr->Left;

			if (parent == nullptr)
			{
				mRoot = child;
				return true;
			}

			if (parent->Left == ptr)
			{
				parent->Left = child;
				child->Parent = parent;
			}
			else if (parent->Right == ptr)
			{
				parent->Right = child;
				child->Parent = parent;
			}

			return true;
		}
		else if (ptr->Left == nullptr)
		{
			shared_ptr<TreeNode<T>> parent = ptr->Parent.lock();
			shared_ptr<TreeNode<T>> child = ptr->Right;
			
			if (parent == nullptr)
			{
				mRoot = child;
				return true;
			}

			if (parent->Left == ptr)
			{
				parent->Left = child;
				child->Parent = parent;
			}
			else if (parent->Right == ptr)
			{
				parent->Right = child;
				child->Parent = parent;
			}

			//if ((*ptr->Data) < (*ptr->Right->Data))
			//{
			//	ptr->Parent.lock()->Right = ptr->Right;
			//}
			//else if ((*ptr->Data) > (*ptr->Right->Data))
			//{
			//	ptr->Parent.lock()->Left = ptr->Right;
			//}
			////ptr->Right->Parent.lock() = nullptr;
			//ptr->Right->Parent = ptr->Parent;
			//ptr = nullptr;

			return true;
		}

		//F1_DeleteExistingReturnsTrue 때문에 시도.
		//return false;
		return true;
	}

	template<typename T> 
	void BinarySearchTree<T>::changeParent(std::shared_ptr<TreeNode<T>> deleteNode, std::shared_ptr<TreeNode<T>> changeNode) 
	{ 
		std::shared_ptr<TreeNode<T>> parent = deleteNode->Parent.lock(); 
		if (parent == nullptr) 
		{
			mRoot = changeNode; 
			return; 
		} 
		if (changeNode == nullptr)
		{ 
			if (parent->Left == deleteNode) 
			{ 
				parent->Left = nullptr; 
			} 
			else if (parent->Right == deleteNode)
			{
				parent->Right = nullptr; 
			} 
		} 
		else 
		{ 
			if (parent->Left == deleteNode) 
			{ 
				parent->Left = changeNode;
			} 
			else if (parent->Right == deleteNode) 
			{ 
				parent->Right = changeNode; 
			} 
			changeNode->Parent = deleteNode->Parent; 
		} 
	}


	//template<typename T>
	//std::shared_ptr<TreeNode<T>> minValueNode(std::shared_ptr<TreeNode<T>> node)
	//{
	//	std::shared_ptr<TreeNode<T>> current = node;
	//	while (current && current->Left != nullptr)
	//	{
	//		current = current->Left;
	//	}
	//	return current;
	//}

	//template<typename T>
	//std::shared_ptr<TreeNode<T>> Delete(std::shared_ptr<TreeNode<T>> parentNode, const T& data)
	//{
	//	if (parentNode == nullptr)
	//	{
	//		return parentNode;

	//		if (data < parentNode->Data)
	//		{
	//			parentNode->Left = delete(parentNode->Left, data);
	//		}
	//		else if (data > parentNode->Data)
	//		{
	//			parentNode->Right = delete(parentNode->swap, data);
	//		}
	//	}
	//	else
	//	{
	//		if (parentNode->Left == nullptr)
	//		{
	//			shared_ptr<TreeNode<T>> temp = parentNode->Right;
	//			parentNode = nullptr;
	//			return temp;
	//		}
	//		else if (parentNode->Right == nullptr)
	//		{
	//			shared_ptr<TreeNode<T>> temp = parentNode->Left;
	//			parentNode = nullptr;
	//			return temp;
	//		}

	//		shared_ptr<TreeNode<T>> temp = minValueNode(parentNode->Right);
	//		parentNode->Data = temp->Data;
	//		parentNode->Right = delete(parentNode->Right, temp->data);
	//	}

	//	return parentNode;
	//}

	template<typename T>
	std::vector<T> BinarySearchTree<T>::TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode)
	{
		std::vector<T> v;
		std::stack<std::shared_ptr<TreeNode<T>>> s;
		std::shared_ptr<TreeNode<T>> curr = startNode;

		while (curr != nullptr || s.empty() == false)
		{
			while (curr != nullptr)
			{
				s.push(curr);
				curr = curr->Left;
			}

			curr = s.top();
			s.pop();

			v.push_back(*curr->Data);

			curr = curr->Right;
		}

		return v;
	}


}
