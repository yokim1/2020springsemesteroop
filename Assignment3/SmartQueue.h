#pragma once
#include <queue>
#include <limits>

using namespace std;

namespace assignment3
{
	template <typename T>
	class SmartQueue
	{
	public:
		SmartQueue<T>();

		void Enqueue(T number);
		T Peek();
		T Dequeue();
		T GetMax();
		T GetMin();
		T GetSum() const;
		double GetAverage();
		double GetVariance();
		double GetStandardDeviation();
		unsigned int GetCount()const;

	private:
		queue<T> mSmartQueue;

		T mTotalValueOfNumber;
		double mTotalSquaredNumber;
	};

	template<typename T>
	inline SmartQueue<T>::SmartQueue()
		: mTotalValueOfNumber(static_cast<T>(0)), mTotalSquaredNumber(static_cast<double>(0))
	{

	}

	template <typename T>
	inline void SmartQueue<T>::Enqueue(T number)
	{
		mSmartQueue.push(number);
		mTotalValueOfNumber = mTotalValueOfNumber + number;
		mTotalSquaredNumber = mTotalSquaredNumber + pow(number, 2);
	}

	template <typename T>
	inline T SmartQueue<T>::Peek()
	{
		return mSmartQueue.front();
	}

	template <typename T>
	inline T SmartQueue<T>::Dequeue()
	{
		T frontNumber = mSmartQueue.front();
		mSmartQueue.pop();
		mTotalValueOfNumber = mTotalValueOfNumber - frontNumber;
		mTotalSquaredNumber = mTotalSquaredNumber - pow(frontNumber, 2);

		return frontNumber;
	}

	template <typename T>
	inline T SmartQueue<T>::GetMax()
	{
		if (mSmartQueue.empty() == true)
		{
			return numeric_limits<T>().lowest();
		}

		unsigned int size = mSmartQueue.size();

		T max = mSmartQueue.front();
		mSmartQueue.pop();
		--size;
		mSmartQueue.push(max);

		while (true)
		{
			if (size == 0)
			{
				break;
			}

			T number = mSmartQueue.front();

			if (max < number)
			{
				max = number;
			}
			--size;
			mSmartQueue.pop();
			mSmartQueue.push(number);
		}

		return max;
	}

	template <typename T>
	inline T SmartQueue<T>::GetMin()
	{
		if (mSmartQueue.empty() == true)
		{
			return numeric_limits<T>().max();
		}

		unsigned int size = mSmartQueue.size();

		T min = mSmartQueue.front();
		mSmartQueue.pop();
		--size;
		mSmartQueue.push(min);

		while (true)
		{
			if (size == 0)
			{
				break;
			}

			T number = mSmartQueue.front();

			if (min > number)
			{
				min = number;
			}
			--size;
			mSmartQueue.pop();
			mSmartQueue.push(number);
		}

		return min;
	}

	template <typename T>
	inline T SmartQueue<T>::GetSum() const
	{
		if (mSmartQueue.empty() == true)
		{
			return static_cast<T>(0);
		}

		return mTotalValueOfNumber;
	}

	template <typename T>
	inline double SmartQueue<T>::GetAverage()
	{
		double avg = static_cast<double>(mTotalValueOfNumber) / mSmartQueue.size();

		double result = round(avg * 1000) / 1000;

		return result;
	}

	template <typename T>
	inline double SmartQueue<T>::GetVariance()
	{
		double avg = static_cast<double>(mTotalValueOfNumber) / mSmartQueue.size();

		double result = static_cast<double>(mTotalSquaredNumber) / mSmartQueue.size() - (avg * avg);

		return round(result * 1000) / 1000;
	}

	template <typename T>
	inline double SmartQueue<T>::GetStandardDeviation()
	{
		double result = this->GetVariance();

		result = sqrt(result);

		return round(result * 1000) / 1000;
	}

	template <typename T>
	inline unsigned int SmartQueue<T>::GetCount() const
	{
		return mSmartQueue.size();
	}
}

























//#pragma once
//#include <queue>
//
//using namespace std;
//
//namespace assignment3
//{
//	template <typename T>
//	
//	class SmartQueue
//	{
//	public:
//		SmartQueue();
//		~SmartQueue();
//		void Enqueue(T number);
//		T Peek() const;
//		T Dequeue();
//		T GetMax()const;
//		T GetMin()const;
//		double GetAverage()const;
//		T GetSum()const;
//		double GetVariance()const;
//		double GetStandardDeviation();
//		unsigned int GetCount()const;
//
//	private:
//		queue<T>* mSmartQueue;
//		queue<T>* mMax;
//		queue<T>* mMin;
//		T mSum;
//
//		double mSquaredSum;
//	};
//
//	template <typename T>
//	SmartQueue <T>::SmartQueue()
//		: mSum(0), mSquaredSum(0)
//	{
//		mSmartQueue = new queue<T>();
//		mMax = new queue<T>();
//		mMin = new queue<T>();
//	}
//
//	template <typename T>
//	SmartQueue <T>::~SmartQueue()
//	{
//		delete mSmartQueue;
//		delete mMax;
//		delete mMin;
//	}
//
//	template <typename T>
//	void SmartQueue<T>::Enqueue(T number)
//	{
//		if (mSmartQueue->size() == 0)
//		{
//			mSmartQueue->push(number);
//			mMax->push(number);
//			mMin->push(number);
//		}
//		else
//		{
//			mSmartQueue->push(number);
//
//			if (mMax->front() < number)
//			{
//				mMax->pop();
//				mMax->push(number);
//			}
//
//			if (mMin->front() > number)
//			{
//				mMin->pop();
//				mMin->push(number);
//			}
//		}
//
//		mSum = mSum + number;
//		mSquaredSum = mSquaredSum + pow(number, 2);
//	}
//
//	template <typename T>
//	T SmartQueue<T>::Peek() const
//	{
//		T frontPeek = mSmartQueue->front();
//
//		return frontPeek;
//	}
//
//	template <typename T>
//	T SmartQueue<T>::Dequeue()
//	{
//		T frontElement = mSmartQueue->front();
//		mSum = mSum - frontElement;
//		mSquaredSum = mSquaredSum - pow(frontElement, 2);
//		mSmartQueue->pop();
//
//		return frontElement;
//	}
//
//	template <typename T>
//	T SmartQueue<T>::GetMax() const
//	{
//		return mMax->front();
//	}
//
//	template <typename T>
//	T SmartQueue<T>::GetMin() const
//	{
//		return mMin->front();
//	}
//
//	template <typename T>
//	double SmartQueue<T>::GetAverage() const
//	{
//		double avg = static_cast<double>(mSum / mSmartQueue->size());
//		avg = round(avg * 1000) / 1000;
//		
//		return avg;
//	}
//
//	template <typename T>
//	T SmartQueue<T>::GetSum() const
//	{
//		return mSum;
//	}
//
//	template <typename T>
//	double SmartQueue<T>::GetVariance() const
//	{
//		double avg = static_cast<double>(mSum / GetCount());
//		double variance = mSquaredSum / GetCount() - pow(avg, 2);
//		
//		variance = round(variance * 1000) / 1000;
//
//		return variance;
//	}
//
//	template <typename T>
//	double SmartQueue<T>::GetStandardDeviation()
//	{
//		double variance = GetVariance();
//		double sd = sqrt(variance);
//
//		sd = round(sd * 1000) / 1000;
//		
//		return sd;
//	}
//
//	template <typename T>
//	unsigned int SmartQueue<T>::GetCount() const
//	{
//		return mSmartQueue->size();
//	}
//
//}