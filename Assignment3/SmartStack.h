#pragma once
#include <stack>
#include <limits>

using namespace std;

namespace assignment3
{
	template<typename T>
	class SmartStack
	{
	public:
		SmartStack();
		SmartStack(const SmartStack&);

		void Push(T number);
		T Pop();
		T Peek();
		T GetMax();
		T GetMin();
		double GetAverage();
		T GetSum() const;
		double GetVariance();
		double GetStandardDeviation();
		unsigned int GetCount() const;

	private:
		stack<T> mSmartStack;
		stack<T> mMax;
		stack<T> mMin;

		T mTotalValueOfNumber;
		double mTotalSquaredNumber;
	};

	template <typename T>
	inline SmartStack<T>::SmartStack()
		: mTotalValueOfNumber(0), mTotalSquaredNumber(0)
	{

	}

	template <typename T>
	inline SmartStack<T>::SmartStack(const SmartStack& other)
		: mTotalValueOfNumber(other.mTotalValueOfNumber), mTotalSquaredNumber(other.mTotalSquaredNumber)
	{
		
	}

	template <typename T>
	inline void SmartStack<T>::Push(T number)
	{
		if (mSmartStack.size() == 0)
		{
			mSmartStack.push(number);
			mMax.push(number);
			mMin.push(number);
		}
		else
		{
			mSmartStack.push(number);

			if (mMax.top() <= number)
			{
				mMax.push(number);
			}

			if (mMin.top() >= number)
			{
				mMin.push(number);
			}
		}

		mTotalValueOfNumber = mTotalValueOfNumber + number;
		mTotalSquaredNumber = mTotalSquaredNumber + pow(number, 2);
	}

	template <typename T>
	inline T SmartStack<T>::Pop()
	{
		T popNumber = mSmartStack.top();
		mSmartStack.pop();

		if (mMax.top() == popNumber)
		{
			mMax.pop();
		}

		if (mMin.top() == popNumber)
		{
			mMin.pop();
		}

		mTotalValueOfNumber = mTotalValueOfNumber - popNumber;
		mTotalSquaredNumber = mTotalSquaredNumber - pow(popNumber, 2);

		//if (mSmartStack.empty())
		//{
		//	mMax.pop();
		//	mMin.pop();

		////	mTotalValueOfNumber = static_cast<T>(0);
		////	mTotalSquaredNumber = static_cast<double>(0);
		//}

		return popNumber;
	}

	template <typename T>
	inline T SmartStack<T>::Peek()
	{
		return mSmartStack.top();
	}

	template <typename T>
	inline T SmartStack<T>::GetMax()
	{
		if (mMax.empty() == true)
		{
			return numeric_limits<T>().lowest();
		}

		return mMax.top();
	}

	template <typename T>
	inline T SmartStack<T>::GetMin()
	{
		if (mMin.size() == 0)
		{
			return numeric_limits<T>().max();
		}

		return mMin.top();
	}

	template <typename T>
	inline double SmartStack<T>::GetAverage()
	{
		if (mSmartStack.empty() == true)
		{
			return static_cast<double>(0);
		}

		double avg = static_cast<double>(mTotalValueOfNumber) / mSmartStack.size();

		double result = round(avg * 1000) / 1000;

		return result;
	}

	template <typename T>
	inline T SmartStack<T>::GetSum() const
	{
		if (mSmartStack.empty() == true)
		{
			return static_cast<T>(0);
		}

		return mTotalValueOfNumber;
	}

	template <typename T>
	inline double SmartStack<T>::GetVariance()
	{
		double avg = static_cast<double>(mTotalValueOfNumber) / mSmartStack.size();

		double result = static_cast<double>(mTotalSquaredNumber) / mSmartStack.size() - (avg * avg);

		return round(result * 1000) / 1000;
	}

	template <typename T>
	inline double SmartStack<T>::GetStandardDeviation()
	{
		double result = this->GetVariance();

		result = sqrt(result);

		return round(result * 1000) / 1000;
	}

	template <typename T>
	inline unsigned int SmartStack<T>::GetCount() const
	{
		return mSmartStack.size();
	}
}





























//#pragma once
//#include <stack>
//#include <limits>
//
//using namespace std;
//
//namespace assignment3
//{
//	template <typename T>
//
//	class SmartStack
//	{	
//	public:
//		SmartStack();
//		~SmartStack();
//		void Push(T number);
//		T Pop();
//		T Peek() const;
//		T GetMax() const;
//		T GetMin() const;
//		double GetAverage();
//		T GetSum() const;
//		double GetVariance() const;
//		double GetStandardDeviation();
//		unsigned int GetCount()const;
//
//	private:
//		stack<T>* mSmartStack;
//		//const CompareArray** mCA;
//		stack<T>* mMax;
//		stack<T>* mMin;
//		T mSum;
//
//		double mSquaredSum;
//	};
//
//
//	template <typename T>
//	SmartStack<T>::SmartStack()
//		: mSum(0), mSquaredSum(0)
//	{
//		mSmartStack = new stack<T>();
//
//		mMax = new stack<T>();
//		mMin = new stack<T>();
//	}
//
//	template <typename T>
//	SmartStack<T>::~SmartStack()
//	{
//		delete mSmartStack;
//		delete mMax;
//		delete mMin;
//	}
//
//	template <typename T>
//	void SmartStack<T>::Push(T number)
//	{
//		if (mSmartStack->size() == 0)
//		{
//			mSmartStack->push(number);
//			mMax->push(number);
//			mMin->push(number);
//		}
//		else
//		{
//			mSmartStack->push(number);
//			
//			if (mMax->top() < number)
//			{
//				//mMax->pop();
//				mMax->push(number);
//			}
//
//			if (mMin->top() > number)
//			{
//				//mMin->pop();
//				mMin->push(number);
//			}
//		}
//		mSum = mSum + number;
//		mSquaredSum = mSquaredSum + pow(number, 2);
//	}
//
//	template <typename T>
//	T SmartStack<T>::Pop()
//	{
//		T topElement = mSmartStack->top();
//		mSmartStack->pop();
//
//		if (mMax->top() == topElement)
//		{
//			mMax->pop();
//		}
//
//		if (mMin->top() == topElement)
//		{
//			mMin->pop();
//		}
//
//		mSum = mSum - topElement;
//		mSquaredSum = mSquaredSum - pow(topElement, 2);
//		
//		return topElement;
//	}
//
//	template <typename T>
//	T SmartStack<T>::Peek() const
//	{
//		T peekNumber = mSmartStack->top();
//		//mSmartStack.push(peekNumber);
//
//		return peekNumber;
//	}
//
//	template <typename T>
//	T SmartStack<T>::GetMax() const
//	{
//		if (mSmartStack->size() == 0)
//		{
//			return std::numeric_limits<T>::min();
//		}
//	
//		return mMax->top();
//	}
//
//	template <typename T>
//	T SmartStack<T>::GetMin() const
//	{
//		if (mSmartStack->size() == 0)
//		{
//			return std::numeric_limits<T>::max();
//		}
//
//		return mMin->top();
//	}
//
//	template <typename T>
//	double SmartStack<T>::GetAverage()
//	{
//		double avg = static_cast<double>(mSum / mSmartStack->size());
//		avg = round(avg * 1000) / 1000;
//		return avg;
//	}
//
//	template <typename T>
//	T SmartStack<T>::GetSum() const
//	{
//		return mSum;
//	}
//
//	template <typename T>
//	double SmartStack<T>::GetVariance() const
//	{
//		//find mean of the value in the stack.
//		
//		double avg = static_cast<double>(mSum / GetCount());
//		double squaredAvg = pow(avg, 2);
//		
//		double variance = mSquaredSum / GetCount() - squaredAvg;
//		variance = round(variance * 1000) / 1000;
//		return variance;
//	}
//
//	template <typename T>
//	double SmartStack<T>::GetStandardDeviation()
//	{	
//		double variance = GetVariance();
//		double sd = sqrt(variance);
//
//		sd = round(sd * 1000) / 1000;
//
//		return sd;
//	}
//
//	template <typename T>
//	unsigned int SmartStack<T>::GetCount() const
//	{
//		return mSmartStack->size();
//	}
//}