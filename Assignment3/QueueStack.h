#pragma once
#include <queue>
#include <stack>
#include <limits>

using namespace std;

namespace assignment3
{
	template <typename T>
	class QueueStack
	{
	public:
		QueueStack();
		QueueStack(unsigned int MaxStackSize);
		QueueStack(const QueueStack<T>& other);
		QueueStack<T>& operator=(const QueueStack<T>& rhs);
		~QueueStack();

		void Enqueue(T number);
		T Peek();
		T Dequeue();
		T GetMax();
		T GetMin();
		double GetAverage();
		T GetSum();
		unsigned int GetCount();
		unsigned int GetStackCount();

	private:
		queue <stack<T>*> mQueueStack;
		stack<T>* mStackPointer;

		unsigned int mMaxStackSize;
		T mTotalValueOfNumber;
		unsigned int mElementCount;
	};

	template<typename T>
	inline QueueStack<T>::QueueStack()
		: mStackPointer(nullptr), mMaxStackSize(0), mTotalValueOfNumber(static_cast<T>(0)), mElementCount(0)
	{

	}

	template<typename T>
	inline QueueStack<T>::QueueStack(unsigned int maxStackSize)
		: mStackPointer(nullptr), mMaxStackSize(maxStackSize), mTotalValueOfNumber(0), mElementCount(0)
	{

	}

	template<typename T>
	inline QueueStack<T>::QueueStack(const QueueStack<T>& other)
		: mStackPointer(nullptr), mMaxStackSize(other.mMaxStackSize), mTotalValueOfNumber(other.mTotalValueOfNumber), mElementCount(other.mElementCount)
	{
		queue<stack<T>*> tmpQueueStack(other.mQueueStack);
		unsigned int queueSize = tmpQueueStack.size();

		while (true)
		{
			if (queueSize == 0)
			{
				break;
			}

			stack<T>* stackFrontPtr = tmpQueueStack.front();
			stack<T>* copyStackPtr = new stack<T>(*stackFrontPtr);

			mQueueStack.push(copyStackPtr);

			tmpQueueStack.pop();
			--queueSize;
		}
	}

	template<typename T>
	inline QueueStack<T>& QueueStack<T>::operator=(const QueueStack<T>& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		mElementCount = rhs.mElementCount;
		mMaxStackSize = rhs.mMaxStackSize;
		mTotalValueOfNumber = rhs.mTotalValueOfNumber;

		queue<stack<T>*> tmpQueueStack(rhs.mQueueStack);
		unsigned int queueSize = tmpQueueStack.size();

		while (true)
		{
			if (queueSize == 0)
			{
				break;
			}

			stack<T>* stackFrontPtr = tmpQueueStack.front();
			stack<T>* copyStackPtr = new stack<T>(*stackFrontPtr);

			mQueueStack.push(copyStackPtr);

			tmpQueueStack.pop();
			--queueSize;
		}

		return *this;
	}

	template<typename T>
	inline QueueStack<T>::~QueueStack()
	{
		while (mQueueStack.empty() != true)
		{
			stack<T>* stackPtr = mQueueStack.front();
			mQueueStack.pop();
			delete stackPtr;
		}
	}

	template <typename T>
	inline void QueueStack<T>::Enqueue(T number)
	{
		if (mQueueStack.size() == 0)
		{
			
			mStackPointer = new stack<T>;
			mStackPointer->push(number);
			mQueueStack.push(mStackPointer);
		}
		else if (mStackPointer->size() == mMaxStackSize)
		{
			
			mStackPointer = new stack<T>;
			mStackPointer->push(number);
			mQueueStack.push(mStackPointer);
		}
		else
		{
			mStackPointer->push(number);
		}

		++mElementCount;
		mTotalValueOfNumber = mTotalValueOfNumber + number;
	}

	template <typename T>
	inline T QueueStack<T>::Peek()
	{
		return mQueueStack.front()->top();
	}

	template <typename T>
	inline T QueueStack<T>::Dequeue()
	{
		T frontQueueTopNumber = mQueueStack.front()->top();

		if (mQueueStack.front()->size() == 1)
		{
			stack<T>* stackPtr = mQueueStack.front();
			mQueueStack.front()->pop();
			mQueueStack.pop();

			delete stackPtr;

			mTotalValueOfNumber = mTotalValueOfNumber - frontQueueTopNumber;
			--mElementCount;

			return frontQueueTopNumber;
		}

		mQueueStack.front()->pop();

		mTotalValueOfNumber = mTotalValueOfNumber - frontQueueTopNumber;
		--mElementCount;

		if (mQueueStack.front()->empty())
		{
			mQueueStack.pop();
		}

		

		return frontQueueTopNumber;
	}

	template <typename T>
	inline T QueueStack<T>::GetMax()
	{
		if (GetCount() == 0)
		{
			return numeric_limits<T>().lowest();
		}

		T max = numeric_limits<T>().lowest();
		unsigned int queueSize = mQueueStack.size();

		while (queueSize != 0)
		{
			stack<T>* queueFrontPtr = mQueueStack.front();
			mQueueStack.pop();
			--queueSize;

			stack<T> tmpStack;

			while (queueFrontPtr->empty() != true)
			{
				T stackTop = queueFrontPtr->top();
				queueFrontPtr->pop();

				if (max < stackTop)
				{
					max = stackTop;
				}

				tmpStack.push(stackTop);
			}

			while (tmpStack.empty() != true)
			{
				T tmpStackTop = tmpStack.top();
				tmpStack.pop();
				queueFrontPtr->push(tmpStackTop);
			}
			mQueueStack.push(queueFrontPtr);
		}

		return max;
	}

	template <typename T>
	inline T QueueStack<T>::GetMin()
	{
		if (GetCount() == 0)
		{
			return numeric_limits<T>().max();
		}

		T min = numeric_limits<T>().max();
		unsigned int queueSize = mQueueStack.size();

		while (queueSize != 0)
		{
			stack<T>* queueFrontPtr = mQueueStack.front();
			mQueueStack.pop();
			--queueSize;

			stack<T> tmpStack;

			while (queueFrontPtr->empty() != true)
			{
				T stackTop = queueFrontPtr->top();
				queueFrontPtr->pop();

				if (min > stackTop)
				{
					min = stackTop;
				}

				tmpStack.push(stackTop);
			}

			while (tmpStack.empty() != true)
			{
				T tmpStackTop = tmpStack.top();
				tmpStack.pop();
				queueFrontPtr->push(tmpStackTop);
			}
			mQueueStack.push(queueFrontPtr);
		}

		return min;
	}

	template <typename T>
	inline double QueueStack<T>::GetAverage()
	{
		double avg = static_cast<double>(mTotalValueOfNumber) / mElementCount;
		avg = round(avg * 1000) / 1000;

		return avg;
	}

	template <typename T>
	inline T QueueStack<T>::GetSum()
	{
		if (mQueueStack.empty())
		{
			return static_cast<T>(0);
		}
		return mTotalValueOfNumber;
	}

	template <typename T>
	inline unsigned int QueueStack<T>::GetCount()
	{
		if (mQueueStack.empty())
		{
			return 0;
		}

		return mElementCount;
	}

	template <typename T>
	inline unsigned int QueueStack<T>::GetStackCount()
	{
		return mQueueStack.size();
	}
}


