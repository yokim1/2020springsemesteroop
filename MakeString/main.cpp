#include <iostream>
#include <string>
#include <random>
#include <algorithm>

using namespace std;

// [문제] STRING은 길이에 관계없이 문자열을 저장한다.
// main이 실행되게 하자

class STRING {
	size_t len; // = unsigned int len
	char* p; // len과 p라는 멤버변수가 있어야 한다. 
public:
	STRING() : len{ 0 }, p{ nullptr }{
		//디폴트 생성자
	}

	STRING(char const* str) { // 쌍따옴표로 묶인것의 자료형-char const* , string 클래스가 만들어짐 , 이 인자를 받아들이는 생성자 함수가 만들어짐
							  // 전달된 str의 글자수를 세서 메모리를 마련한 후 저장
		len = strlen(str); // strlen - 몇글자인지 알려주는 함수
		//정해지지 않은 메모리를 잡는 방법
		p = new char[len];
		memcpy(p, str, len); // 복사생성 2번재있는 데이터를 1번째로 옮겨라 

	}

	// 소멸자 생성시 자원을 확보했다면 반드시 
	// 소멸자에서 확보한 자원을 되돌려줘야 한다

	~STRING()
	{
		if (len != 0)
			delete[]p;
	}

	// 복사생성자
	STRING(const STRING& other) : len{ other.len } {
		p = new char[len];
		//깊은 복사를 해야 dangling pointer가 안만들어진다.
		memcpy(p, other.p, len);

	}

	// copy assignment operator
	STRING& operator = (const STRING& rhs) {
		// a=a; 이럴땐 아무일도 안해야 함
		if (this == &rhs)
			return *this;

		//확보한 메모리를 해제한다
		if (len != 0)
			delete[]p;

		len = rhs.len;
		p = new char[len];
		memcpy(p, rhs.p, len);

	}


	// 이동생성자
	STRING(STRING&& other) noexcept : len{ other.len }, p{ other.p } {

		other.len = 0;
		other.p = nullptr;

	}//앞에 const 쓰면 원본을 읽기만 하겠따는 뜻이라 못씀

	// 이동할당연산자
	// noexcept - 예외없음, 복사시킴
	STRING& operator=(STRING&& other) noexcept {
		if (this == &other)
			return *this;

		if (len != 0)
			delete[] p;

		len = other.len;

		p = other.p;

		other.len = 0;
		other.p = nullptr;

		return *this;

	}

	STRING operator+(const STRING& rhs)const {
		STRING res;
		res.len = len + rhs.len;
		res.p = new char[res.len];

		memcpy(res.p, p, len);
		memcpy(res.p + len, rhs.p, rhs.len); // ~에다가 ~에서 ~만큼

		return res;
	}

	STRING operator+(char c) {

		STRING res;
		res.len = len + 1;
		res.p = new char[res.len];

		memcpy(res.p, p, len);
		res.p[res.len - 1] = c;

		return res;

	}


	char operator[](int idx) const { // 읽기용
		return *(p + idx);
	}

	char operator[](int idx) { // 위에거랑 const 로 구분가능 - 쓰기용
		return *(p + idx);

	} //배열 연산자


	size_t length() const {
		return len;
	}

	char* begin() const {
		return p;
	}
	char* end() const {
		return p + len;
	}

	friend ostream& operator<<(ostream&, const STRING&);


};

ostream& operator<<(ostream& os, const STRING& s)
{
	for (int i = 0; i < s.len; ++i)
	{
		os << *(s.p + i);
	}

	return os;
};

default_random_engine dre;
uniform_int_distribution<> uidChar{ 'a','z' };
uniform_int_distribution<> uidNum{ '1','70' };

// 모두 대문자로 저장하는 STRING 클래스를 만들어라

int main()
{
	STRING str[100];

	STRING a;
	STRING b{ "hello" };

	//a = a + 'c';

	cout << b << endl;

	//for (int i = 0; i < 10; ++i)
	//	//int num = uidNum(dre);
	//{
	//	for (int j = 0; j < uidNum(dre); ++j)
	//		str[i] = str[i] + uidChar(dre);
	//}

	//// str을 길이기준 오름차순으로 정렬하자
	//sort(begin(str), end(str), [](const STRING& a, STRING& b) {
	//	return a.length() < b.length();
	//});

	//cout << str[0] << endl;

	system("pause");

}