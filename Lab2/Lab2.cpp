﻿#include "Lab2.h"

namespace lab2
{
	

	char DecideSign(float inputNumber) 
	{
		if (inputNumber < 0)
		{
			return '-';
		}
		else
		{
			return '+';
		}
	}

	float DecideMax(float max, float inputNumber)
	{
		if (max < inputNumber)
		{
			return inputNumber;
		}
		else
		{
			return max;
		}
	}

	void PrintIntegers(std::istream& in, std::ostream& out)
	{
		int inputNumber;
		string trash;
		int tmpInputNumber = 0;
		//int whilecnt = 0;

		//12 oct spaces, 11 dec spaces, 9 hex spaces

		out << setfill(' ') << setw(12) << "oct";
		out << setfill(' ') << setw(11) << "dec";
		out << setfill(' ') << setw(9) << "hex" << endl;
		out << "------------" << " " << "----------" << " " << "--------" << endl;

		while (true) 
		{
			in >> inputNumber;

			if (!in.fail()) 
			{
				if (tmpInputNumber != inputNumber)
				{
					tmpInputNumber = inputNumber;
				}
				else if (tmpInputNumber == inputNumber)
				{
					break;
				}

				//++whilecnt;

				out << setw(12) << right << oct << inputNumber;
				out << setw(11) << right << dec << inputNumber;
				out << setw(9) << right << uppercase << hex << inputNumber << endl;

				/*if (whilecnt == 5)
				{
					break;
				}*/
				continue;
			}

			if (in.eof())
			{
				break;
			}

			in.clear();
			in >> trash;
			//--whilecnt;
		}
	}

	void PrintMaxFloat(std::istream& in, std::ostream& out)
	{
		//int whilecnt = 0;
		float inputNumber;
		float tmpNumber = 0;
		float max = 0;
		char sign = '+';
		string trash;

		while (true) 
		{
			in >> inputNumber;

			max = DecideMax(max, inputNumber);

			if (!in.fail()) 
			{
				if (tmpNumber != inputNumber)
				{
					tmpNumber = inputNumber;
				}
				else if (tmpNumber == inputNumber)
				{
					break;
				}

				sign = DecideSign(inputNumber);

				if (inputNumber < 0)
				{
					inputNumber = inputNumber * -1;
				}

				//NodeMaker(L, tmpNumber);

				out << "     " << sign;
				out << right << setw(14) << fixed << setprecision(3) << inputNumber << endl;

				//++whilecnt;

				/*if (whilecnt == 6)
				{
					break;
				}*/
				continue;
			}

			if (in.eof())
			{
				break;
			}

			in.clear();
			in >> trash;
		}
		out << "max: " << sign;
		out << right << setw(14) << fixed << setprecision(3) << max << endl;
	}
}