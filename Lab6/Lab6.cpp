#include "Lab6.h"

namespace lab6
{
	int Sum(const std::vector<int>& v)
	{
		int result = 0;
		int size = v.size();

		for (int i = 0; i < size; ++i)
		{
			result += v[i];
		}

		return result;
	}

	int Min(const std::vector<int>& v)
	{
		int size = v.size();

		if (size <= 0)
		{
			return INT_MAX;
		}

		int min = v[0];

		for (int i = 1; i < size; ++i)
		{
			if (min > v[i])
			{
				min = v[i];
			}
		}

		return min;
	}

	int Max(const std::vector<int>& v)
	{
		int size = v.size();

		if (size <= 0)
		{
			return INT_MIN;
		}

		int max = v[0];
		
		for (int i = 1; i < size; ++i)
		{
			if (max < v[i])
			{
				max = v[i];
			}
		}

		return max;
	}

	float Average(const std::vector<int>& v)
	{
		float size = static_cast<float>(v.size());

		if (size <= 0)
		{
			return 0;
		}

		float sum = static_cast<float>(Sum(v));
		float avg = 0;
		
		avg = sum / size;

		return avg;
	}

	int NumberWithMaxOccurrence(const std::vector<int>& v)
	{
		int size = v.size();

		if (size <= 0)
		{
			return 0;
		}

		int cnter = 0;
		int compareNumber;
		
		int maxOccurence = 0;
		int maxOccurenceNumber;

		for (int i = 0; i < size; ++i)
		{
			compareNumber = v[i];
			cnter = 0;

			for (int j = 0; j < size; ++j)
			{
				if (compareNumber == v[j])
				{
					++cnter;
				}
			}

			if (maxOccurence < cnter)
			{
				maxOccurence = cnter;
				maxOccurenceNumber = compareNumber;
			}
		}
	
		return maxOccurenceNumber;
	}

	void SortDescending(std::vector<int>& v)
	{
		int size = v.size();

		if (size <= 0)
		{
			return;
		}
		
		int tmp;

		for (int i = 0; i < size - 1; ++i)
		{
			for (int j = 0; j < size - i - 1; ++j)
			{
				if (v[j] < v[j + 1])
				{
					tmp = v[j];
					v[j] = v[j + 1];
					v[j + 1] = tmp;
				}
			}
		}
	}

}