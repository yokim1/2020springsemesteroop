#pragma once

#include <memory>
//#include <algorithm>
//#include <numeric>

namespace lab11
{
	template<typename T>
	class Storage
	{
	public:
		Storage(unsigned int length);
		Storage(unsigned int length, const T& initialValue);

		Storage(const Storage<T>& other);

		//이동 생성자
		Storage(Storage<T>&& other);

		Storage<T>& operator=(const Storage<T>& rhs);

		//이동 대입 연산자
		Storage<T>& operator=(Storage<T>&& rhs);

		bool Update(unsigned int index, const T& data);
		const std::unique_ptr<T[]>& GetData() const;
		unsigned int GetSize() const;

	private:
		std::unique_ptr<T[]> mArray;
		unsigned int mSize;
	};

	template<typename T>
	Storage<T>::Storage(unsigned int length)
		: mArray(nullptr), mSize(length)
	{
		mArray = std::make_unique<T[]>(mSize);

		//for (unsigned int i = 0; i < mSize; ++i)
		//{
		//	mArray[i] = 0;
		//}
	}

	template<typename T>
	Storage<T>::Storage(unsigned int length, const T& initialValue)
		: mSize(length)
	{
		mArray = std::make_unique<T[]>(mSize);

		for (unsigned int i = 0; i < mSize; ++i)
		{
			mArray[i] = initialValue;
		}
	}

	template<typename T>
	Storage<T>::Storage(const Storage<T>& other)
		: mSize(other.mSize)
	{
		mArray = std::make_unique<T[]>(mSize);
		
		for (unsigned int i = 0; i < mSize; ++i)
		{
			mArray[i] = other.mArray[i];
		}
	}

	template<typename T>
	Storage<T>::Storage(Storage<T>&& other)
		: mArray(std::move(other.mArray)), mSize(other.mSize)
	{
		other.mArray = nullptr;
		other.mSize = 0;
	}

	template<typename T>
	Storage<T>& Storage<T>::operator=(const Storage& rhs)
	{
		if (this != &rhs)
		{
			mArray.reset(new T[rhs.mSize]);
			mSize = rhs.mSize;

			for (unsigned int i = 0; i < mSize; ++i)
			{
				mArray[i] = rhs.mArray[i];
			}
		}
		return *this;
	}

	template<typename T>
	Storage<T>& Storage<T>::operator=(Storage&& rhs)
	{
		if (this != &rhs)
		{
			mArray.reset();

			mArray = std::move(rhs.mArray);
			mSize = rhs.mSize;

			rhs.mArray = nullptr;
			rhs.mSize = 0;
		}
		return *this;
	}

	template<typename T>
	bool Storage<T>::Update(unsigned int index, const T& data)
	{
		if (mSize <= index)
		{
			return false;
		}

		mArray[index] = data;
		return true;
	}

	template<typename T>
	const std::unique_ptr<T[]>& Storage<T>::GetData() const
	{
		/*std::unique_ptr<T[]> temp = std::make_unique<T[]>(1);
		return std::move(temp);*/

		return mArray;
	}

	template<typename T>
	unsigned int Storage<T>::GetSize() const
	{
		return mSize;
	}
}