#pragma once

using namespace std;

namespace lab8
{
	template<size_t N>
	class FixedVector<bool, N>
	{
	public:
		FixedVector();
		FixedVector(const FixedVector<bool, N>& other);
		FixedVector<bool, N>& operator=(const FixedVector<bool, N>& rhs);
		~FixedVector();

		bool Add(bool data);
		bool Remove(bool data);
		bool Get(unsigned int index);
		bool operator[](unsigned int index);
		int GetIndex(bool data);
		size_t GetSize();
		size_t GetCapacity();

	private:
		enum { MAX = N };
		size_t mSize;
		bool mbArray[MAX];
	};

	template<size_t N>
	FixedVector<bool, N>::FixedVector()
		: mSize(0)
	{

	}

	template<size_t N>
	FixedVector<bool, N>::FixedVector(const FixedVector<bool, N>& bOther)
		: mSize(bOther.mSize), mbArray(bOther.mbArray)
	{

	}

	template<size_t N>
	FixedVector<bool, N>& FixedVector<bool, N>::operator=(const FixedVector<bool, N>& bRhs)
	{
		mSize = bRhs.mSize;
		mbArray = bRhs.mbArray;
	}

	template<size_t N>
	FixedVector<bool, N>::~FixedVector()
	{

	}

	template<size_t N>
	bool FixedVector<bool, N>::Add(bool bData)
	{
		if (mSize == MAX)
		{
			return false;
		}

		mbArray[mSize] = bData;
		++mSize;

		return true;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Remove(bool bData)
	{
		for (unsigned int i = 0; i < mSize; ++i)
		{
			if (mbArray[i] == bData)
			{
				for (unsigned int j = i; j < mSize; ++j)
				{
					mbArray[j] = mbArray[j + 1];

				}

				--mSize;

				return true;
			}
		}
		return false;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Get(unsigned int index)
	{
		return mbArray[index];
	}

	template<size_t N>
	bool FixedVector<bool, N>::operator[](unsigned int index)
	{
		return mbArray[index];
	}

	template<size_t N>
	int FixedVector<bool, N>::GetIndex(bool bData)
	{
		for (unsigned int i = 0; i < mSize; ++i)
		{
			if (mbArray[i] == bData)
			{
				return i;
			}
		}

		return -1;
	}

	template<size_t N>
	size_t FixedVector<bool, N>::GetSize()
	{
		return mSize;
	}

	template<size_t N>
	size_t FixedVector<bool, N>::GetCapacity()
	{
		return N;
	}
}
