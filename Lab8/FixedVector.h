#pragma once

using namespace std;

namespace lab8
{
	template <typename T, size_t N>
	class FixedVector
	{
	public:
		FixedVector();
		FixedVector(const FixedVector<T, N>& other);
		FixedVector<T, N>& operator=(const FixedVector<T, N>& rhs);
		~FixedVector();

		bool Add(const T& data);
		bool Remove(const T& data);
		const T& Get(unsigned int index);
		T& operator[](unsigned int index);
		int GetIndex(const T& data);
		size_t GetSize() const;
		size_t GetCapacity() const;

	private:
		enum { MAX = N };
		size_t mSize;
		T mArray[MAX];
		unsigned int mCurrPos;
	};

	template <typename T, size_t N>
	inline FixedVector<T, N>::FixedVector()
		: mSize(0), mCurrPos(0)
	{

	}

	template <typename T, size_t N>
	inline FixedVector<T, N>::FixedVector(const FixedVector<T, N>& other)
		: mSize(other.mSize), mCurrPos(other.mCurrPos)
	{
		for (unsigned int i = 0; i < mSize; ++i)
		{
			mArray[i] = other.mArray[i];
		}
	}

	template <typename T, size_t N>
	inline FixedVector<T, N>& FixedVector<T, N>::operator=(const FixedVector<T, N>& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		mSize = rhs.mSize;
		mCurrPos = rhs.mCurrPos;

		for (unsigned int i = 0; i < mSize; ++i)
		{
			mArray[i] = rhs.mArray[i];
		}
		return *this;
	}

	template <typename T, size_t N>
	inline FixedVector<T, N>::~FixedVector()
	{

	}

	template <typename T, size_t N>
	inline bool FixedVector<T, N>::Add(const T& input)
	{
		if (mSize == MAX)
		{
			return false;
		}

		mArray[mCurrPos] = input;
		++mCurrPos;
		++mSize;

		return true;
	}

	//mCurrPos를 어떻게 해야할지 고민.
	template <typename T, size_t N>
	inline bool FixedVector<T, N>::Remove(const T& input)
	{
		for (unsigned int i = 0; i < mSize; ++i)
		{
			if (input == mArray[i])
			{
				for (unsigned int j = i; j < mSize; ++j)
				{
					mArray[j] = mArray[j + 1];
				}
				--mCurrPos;
				--mSize;

				return true;
			}
		}

		return false;
	}

	template <typename T, size_t N>
	inline const T& FixedVector<T, N>::Get(unsigned int index)
	{
		return mArray[index];
	}

	template <typename T, size_t N>
	inline T& FixedVector<T, N>::operator[](unsigned int index)
	{
		return mArray[index];
	}

	template <typename T, size_t N>
	inline int FixedVector<T, N>::GetIndex(const T& input)
	{
		for (unsigned int i = 0; i < mSize; ++i)
		{
			if (mArray[i] == input)
			{
				return i;
			}
		}

		return -1;
	}

	template <typename T, size_t N>
	inline size_t FixedVector<T, N>::GetSize() const
	{
		return mSize;
	}

	template <typename T, size_t N>
	inline size_t FixedVector<T, N>::GetCapacity() const
	{
		return MAX;
	}
}