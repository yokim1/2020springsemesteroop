#pragma once

#include <iostream>
#include <vector>
#include <map>

namespace lab7
{
	template <typename K, class V>
	std::map<K, V> ConvertVectorsToMap(const std::vector<K>& keys, const std::vector<V>& values)
	{
		std::map<K, V> m;

		int size = 0;

		if (keys.size() < values.size())
		{
			size = keys.size();
		}
		else
		{
			size = values.size();
		}


		for (int i = 0; i < size; ++i)
		{
			m.insert(std::make_pair(keys[i], values[i]));
		}

		return m;
	}

	template <typename K, class V>
	std::vector<K> GetKeys(const std::map<K, V>& m)
	{
		std::vector<K> v;
		
		
		for (auto i = m.begin(); i != m.end(); ++i)
		{
			v.push_back(i->first);
		}
		
		return v;
	}

	template <typename K, class V>
	std::vector<V> GetValues(const std::map<K, V>& m)
	{
		std::vector<V> v;

		for (auto i = m.begin(); i != m.end(); ++i)
		{
			v.push_back(i->second);
		}

		return v;
	}

	template <typename T>
	std::vector<T> Reverse(const std::vector<T>& v)
	{
		std::vector<T> rv;

		for (auto i = v.rbegin(); i != v.rend(); ++i)
		{
			rv.push_back(*i);
		}

		return rv;
	}

	template <typename T>
	std::vector<T> operator+(const std::vector<T>& v1, const std::vector<T>& v2)
	{
		std::vector<T> combined;

		//push_back하기 전에 중복 검사후 넣을지 그냥 패스할지 결정한다.
		
		bool bV1check = true;
		bool bV2check = true;

		if (v1.size() != 0)
		{
			for (auto i = v1.begin(); i != v1.end(); ++i)
			{
				for (auto j = combined.begin(); j != combined.end(); ++j)
				{
					if (*i != *j)
					{
						bV1check = true;
					}
					else
					{
						bV1check = false;
						break;
					}
				}

				if (bV1check == true)
				{
					combined.push_back(*i);
					bV1check = false;
				}
			}
		}

		if (v2.size() != 0)
		{
			for (auto i = v2.begin(); i != v2.end(); ++i)
			{
				for (auto j = combined.begin(); j != combined.end(); ++j)
				{
					if (*i != *j)
					{
						bV2check = true;
					}
					else
					{
						bV2check = false;
						break;
					}
				}

				if (bV2check == true)
				{
					combined.push_back(*i);
					bV2check = false;
				}
			}
		}

		return combined;
	}

	template <typename K, class V>
	std::map<K, V> operator+(const std::map<K, V>& m1, const std::map<K, V>& m2)
	{
		std::map<K, V> combined;

		for (auto i = m1.begin(); i != m1.end(); ++i)
		{
			combined.insert(std::make_pair(i->first, i->second));
		}
		for (auto i = m2.begin(); i != m2.end(); ++i)
		{
			combined.insert(std::make_pair(i->first, i->second));
		}

		return combined;
	}

	template<typename T>
	std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
	{
		for (unsigned int i = 0; i < v.size(); ++i)
		{
			os << v[i];

			if (i != v.size() - 1)
			{
				os << ", ";
			}
		}

		return os;
	}

	template <typename K, class V>
	std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m)
	{
		for (auto i = m.begin(); i != m.end(); ++i)
		{
			os << "{ " << i->first << ", " << i->second << " }" << std::endl;
		}

		return os;
	}
}