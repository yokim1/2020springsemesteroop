#pragma once

#include <memory>
using namespace std;

namespace lab10
{
	template<typename T>
	class Node;

	template<typename T>
	class DoublyLinkedList
	{
	public:
		DoublyLinkedList();
		void Insert(std::unique_ptr<T> data);
		void Insert(std::unique_ptr<T> data, unsigned int index);
		bool Delete(const T& data);
		bool Search(const T& data) const;

		std::shared_ptr<Node<T>> operator[](unsigned int index) const;
		unsigned int GetLength() const;

	private:
		std::shared_ptr<Node<T>> mHead;
		/*std::shared_ptr<Node<T>> mTail;
		std::shared_ptr<Node<T>> mPtr;*/
		unsigned int mSize;
	};

	template<typename T>
	DoublyLinkedList<T>::DoublyLinkedList()
		: mHead(nullptr), mSize(0)
	{
		
	}

	template<typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data)
	{

		shared_ptr<Node<T>>ptr = std::make_shared<Node<T>>(Node<T>(std::move(data)));
		shared_ptr<Node<T>>mark = mHead;

		if (mHead == nullptr)
		{
			mHead = ptr;
			mark = ptr;
			//mTail = mPtr;
			++mSize;
			return;
		}

		while (mark->Next != nullptr)
		{
			mark = mark->Next;
		}

		/*mTail->Next = mPtr;
		mPtr->Previous = mTail;
		mTail = mPtr;*/

		mark->Next = ptr;
		ptr->Previous = mark;
		ptr->Next = nullptr;

		++mSize;
	}

	template<typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data, unsigned int index)
	{
		
		if (mSize - 1 < index)
		{
			this->Insert(move(data));
			return;
		}
		
		shared_ptr<Node<T>>ptr = make_shared<Node<T>>(Node<T>(std::move(data)));
		shared_ptr<Node<T>>mark = mHead;
		unsigned int i = 0;

		if (mHead == nullptr)
		{
			mHead = ptr;
			mark = ptr;
			//mTail = mPtr;
			return;
		}

		while (i != index)
		{
			/*if (Mark->Next == nullptr)
			{
				break;
			}*/

			++i;
			mark = mark->Next;
		}

		if (index == 0)
		{
			mHead->Previous = ptr;
			ptr->Next = mHead;
			mHead = ptr;
			mark = ptr;

			++mSize;
			return;
		}
		
		ptr->Previous = mark->Previous.lock();
		ptr->Next = mark;
		mark->Previous.lock()->Next = ptr;
		mark->Previous = ptr;
		++mSize;
		////맨 앞에 넣기
		//if (index == 0)
		//{
		//	mPtr->Next = mHead;
		//	mHead->Previous = mPtr;
		//	mHead = mPtr;
		//	++mSize;
		//	return;
		//}

		//
		//while (i < index )
		//{
		//	markerPtr = markerPtr->Next;
		//	++i;
		//}

		//mPtr->Next = markerPtr->Previous.lock();
		//markerPtr->Previous.lock()->Next = mPtr;
		//mPtr->Next = markerPtr;
		//markerPtr->Previous = mPtr;
		//++mSize;
		//

		////맨 앞에 넣기
		//if (index == 0)
		//{
		//	mPtr->Next = mHead;
		//	mHead->Previous = mPtr;
		//	mHead = mPtr;
		//	++mSize;
		//	return;
		//}

		////맨 뒤에 넣기
		//if (index == mSize || index >= mSize)
		//{
		//	mPtr->Previous = mTail;
		//	mTail->Next = mPtr;
		//	mTail = mPtr;
		//	++mSize;
		//	return;
		//}

		////중간에 넣기

		//while (i != index)
		//{
		//	markerPtr = markerPtr->Next;
		//	++i;
		//}
		//
		//
		//markerPtr->Previous.lock()->Next = mPtr;
		//mPtr->Previous = markerPtr->Previous.lock();
		//
		//markerPtr->Previous = mPtr;
		//mPtr->Next = markerPtr;
		//
		//++mSize;
	}

	template<typename T>
	bool DoublyLinkedList<T>::Delete(const T& data)
	{
		shared_ptr<Node<T>> ptr = mHead;

		while (ptr != nullptr)
		{
			if (*(ptr->Data) == data)
			{
				break;
			}
			ptr = ptr->Next;
		}

		if (ptr == nullptr)
		{
			return false;
		}

		if (mSize == 1)
		{
			mHead = nullptr;
			//mTail = nullptr;
			ptr->Next = nullptr;
			ptr->Previous.lock() = nullptr;
			--mSize;
			return true;
		}

		//if it was at the head
		else if (ptr == mHead)
		{
			ptr->Next->Previous.lock() = nullptr;
			mHead = ptr->Next;
			ptr->Next = nullptr;
			--mSize;
			return true;
		}
		//// if it was at the tail
		//else if (mPtr == mTail)
		//{
		//	mPtr->Next = nullptr;
		//	mTail = mPtr->Previous.lock();
		//	mPtr->Previous.lock() = nullptr;
		//	--mSize;
		//	return true;
		//}

		ptr->Previous.lock()->Next = ptr->Next;
		ptr->Previous.lock() = nullptr;

		if (ptr->Next != nullptr)
		{
			ptr->Next->Previous = ptr->Previous.lock();
			ptr->Next = nullptr;
		}
		--mSize;
		return true;
		
	}

	template<typename T>
	bool DoublyLinkedList<T>::Search(const T& data) const
	{
		std::shared_ptr<Node<T>> indexFinder = mHead;

		while (indexFinder != nullptr)
		{
			if (*(indexFinder->Data) == data)
			{
				return true;
			}
			indexFinder = indexFinder->Next;
		}

		return false;
	}

	template<typename T>
	std::shared_ptr<Node<T>> DoublyLinkedList<T>::operator[](unsigned int index) const
	{
		if (index >= mSize)
		{
			return nullptr;
		}

		unsigned int i = 0;
		std::shared_ptr<Node<T>> indexFinder = mHead;

		while (i != index)
		{
			indexFinder = indexFinder->Next;
			++i;
		}

		return indexFinder;

		/*auto x = std::make_unique<T>();
		std::shared_ptr<Node<T>> temp = std::make_shared<Node<T>>(std::move(x));
		return temp;*/
	}

	template<typename T>
	unsigned int DoublyLinkedList<T>::GetLength() const
	{
		return mSize;
	}
}