#include <cassert>
#include <crtdbg.h>
#include "DoublyLinkedList.h"
#include "Node.h"

#ifdef _DEBUG
#define new new(_CLIENT_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

using namespace lab10;

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	DoublyLinkedList<int> list;

	assert(list[0] == nullptr);
	assert(list[3] == nullptr);
	bool bEmptyDeleted = list.Delete(6);
	assert(!bEmptyDeleted);
	bool bEmptySearched = list.Search(7);
	assert(!bEmptySearched);

	list.Insert(std::make_unique<int>(10));
	list.Insert(std::make_unique<int>(20));
	list.Insert(std::make_unique<int>(30));
	list.Insert(std::make_unique<int>(40));
	list.Insert(std::make_unique<int>(50));
	list.Insert(std::make_unique<int>(60));
	list.Insert(std::make_unique<int>(70));

	std::shared_ptr<Node<int>> node;
	list.Insert(std::make_unique<int>(12), 7);
	assert(list.GetLength() == 8);
	node = list[7];
	assert(*node->Data == 12);

	//bool bSearched = list.Search(40);
	//assert(bSearched);

	//bSearched = list.Search(10);
	//assert(bSearched);

	//bSearched = list.Search(70);
	//assert(bSearched);

	//bSearched = list.Search(100);
	//assert(!bSearched);

	//unsigned int size = list.GetLength();
	//assert(size == 7);

	//bool bDeleted = list.Delete(30);
	//assert(bDeleted);

	//bDeleted = list.Delete(30);
	//assert(!bDeleted);

	//size = list.GetLength();
	//assert(size == 6);

	//std::shared_ptr<Node<int>> node = list[2];
	//assert(*node->Data == 40);

	//list.Insert(std::make_unique<int>(100), 2);

	//node = list[2];
	//assert(*node->Data == 100);

	//bDeleted = list.Delete(10);
	//assert(bDeleted);
	//assert(*list[0]->Data == 20);

	//bDeleted = list.Delete(70);
	//assert(bDeleted);

	//list.Insert(std::make_unique<int>(110), 0);

	//node = list[0];
	//assert(*node->Data == 110);
	//assert(list.GetLength() == 6);

	//list.Insert(std::make_unique<int>(12), 6);
	//assert(list.GetLength() == 7);
	//node = list[6];
	//assert(*node->Data == 12);

	//list.Insert(std::make_unique<int>(13), 9);
	//assert(list.GetLength() == 8);
	//node = list[7];
	//assert(*node->Data == 13);

	//bDeleted = list.Delete(20);
	//assert(bDeleted);
	//bDeleted = list.Delete(40);
	//assert(bDeleted);
	//bDeleted = list.Delete(50);
	//assert(bDeleted);
	//bDeleted = list.Delete(60);
	//assert(bDeleted);
	//bDeleted = list.Delete(100);
	//assert(bDeleted);
	//bDeleted = list.Delete(110);
	//assert(bDeleted);
	//bDeleted = list.Delete(12);
	//assert(bDeleted);
	//bDeleted = list.Delete(13);
	//assert(bDeleted);

	//assert(list[0] == nullptr);

	return 0;
}