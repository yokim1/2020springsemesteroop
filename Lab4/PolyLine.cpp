#include <cstring>
#include <cmath>
#include "PolyLine.h"
#include <iostream>
using namespace std;

namespace lab4
{
	PolyLine::PolyLine() : mCnt(0)
	{
		cout << this << "PolyLine(d)" << endl;

		mPolyArrAddr = new const Point*[mCntLimit];
		
		for (int i = 0; i < mCntLimit; ++i)
		{
			mPolyArrAddr[i] = nullptr;
		}
	}

	PolyLine::PolyLine(const PolyLine& other)
		: mCnt(other.mCnt)
	{
		cout << this << "PolyLine(const PolyLine&)" << endl;
		
		delete mPolyArrAddr;
		
		mPolyArrAddr = new const Point*[mCntLimit];
		
		for (int i = 0; i < mCnt; ++i) 
		{
			mPolyArrAddr[i] = new const Point(*(other.mPolyArrAddr[i]));
		}
	}

	PolyLine::~PolyLine()
	{
		/*if (mCnt != 0) 
		{*/
		cout << this << "~PolyLine()" << endl;
			
		for (int i = 0; i < mCnt; ++i)
		{
			delete mPolyArrAddr[i];
		}
		delete[] mPolyArrAddr;
		//}
	}

	bool PolyLine::AddPoint(float x, float y)
	{
		if (mCntLimit <= mCnt)
		{
			return false;
		}
		else 
		{
			mPolyArrAddr[mCnt] = new Point(x, y);
			++mCnt;
			return true;
		}
		return false;
	}

	bool PolyLine::AddPoint(const Point* point)
	{
		if (-1 >= mCnt || mCnt >= 10)
		{
			return false;
		}
		else 
		{
			mPolyArrAddr[mCnt] = point;
			++mCnt;
			return true;
		}
		return false;
	}

	bool PolyLine::RemovePoint(unsigned int i)
	{
		int deleteIndex = static_cast<int>(i);

		if (mCnt - 1 < deleteIndex || deleteIndex <= -1)
		{
			return false;
		}
		else 
		{
			delete mPolyArrAddr[deleteIndex];
			mPolyArrAddr[deleteIndex] = nullptr;

			for (int i = deleteIndex; i < mCnt; ++i)
			{
				mPolyArrAddr[i] = mPolyArrAddr[i + 1];
			}

			for (int i = mCnt; i < mCntLimit; ++i)
			{
				delete mPolyArrAddr[i];
				mPolyArrAddr[i] = nullptr;
			}

			--mCnt;

			return true;
		}
		return false;
	}

	bool PolyLine::TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const
	{
		if (mCnt <= 0)
		{
			return false;
		}
		else
		{
			float minX = FLT_MAX;
			float maxX = FLT_MIN;

			float minY = FLT_MAX;
			float maxY = FLT_MIN;


			for (int i = 0; i < mCnt; ++i)
			{
				if (minX > mPolyArrAddr[i]->GetX())
				{
					minX = mPolyArrAddr[i]->GetX();
				}

				if (minY > mPolyArrAddr[i]->GetY())
				{
					minY = mPolyArrAddr[i]->GetY();
				}

				if (maxX < mPolyArrAddr[i]->GetX())
				{
					maxX = mPolyArrAddr[i]->GetX();
				}

				if (maxY < mPolyArrAddr[i]->GetY())
				{
					maxY = mPolyArrAddr[i]->GetY();
				}
			}

			Point min(minX, minY);
			Point max(maxX, maxY);

			*outMin = min;
			*outMax = max;

			return true;
		}
		return false;
	}

	const Point* PolyLine::operator[](unsigned int i) const
	{
		int index = static_cast<int>(i);

		if (mCnt <= index || index <= -1)
		{
			return nullptr;
		}
		else 
		{
			//return new Point(mPolyArrAddr[index]->GetX(), mPolyArrAddr[index]->GetY());
			return mPolyArrAddr[i];
		}
		return nullptr;
	}

	PolyLine& PolyLine::operator=(const PolyLine& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}
		else
		{
			delete[] mPolyArrAddr;
			mPolyArrAddr = new const Point* [mCntLimit];
			mCnt = rhs.mCnt;

			for (int i = 0; i < mCntLimit; ++i)
			{
				mPolyArrAddr[i] = nullptr;
			}
			
			for (int i = 0; i < mCnt; ++i)
			{
				mPolyArrAddr[i] = new const Point(*(rhs.mPolyArrAddr[i]));
			}
			
		}
		return *this;
		
	}
}












































//#include <cstring>
//#include <cmath>
//#include <iostream>
//#include "PolyLine.h"
//
//namespace lab4
//{
//	PolyLine::PolyLine()
//	{
//		mP = new Point[mMax];
//	}
//
//	PolyLine::PolyLine(const PolyLine& other)
//	{
//		mP = other.mP;
//		mCnt = other.mCnt;
//	}
//
//	PolyLine::~PolyLine()
//	{
//		delete[] mP;
//	}
//
//	bool PolyLine::AddPoint(float x, float y)
//	{
//		return AddPoint(new Point(x, y));
//	}
//
//	bool PolyLine::AddPoint(const Point* point)
//	{
//		if (mCnt == 10) 
//		{
//			return false;
//		}
//		else
//		{
//			mP[mCnt] = *point;
//			++mCnt;
//			return true;
//		}
//	}
//
//	bool PolyLine::RemovePoint(unsigned int i)
//	{
//		int index = static_cast<int>(i);
//		if (mCnt < index || index <= -1) 
//		{
//			return false;
//		}
//		else 
//		{
//			for(; index < mCnt; ++index)
//			{
//				mP[index] = mP[index + 1];
//			}
//			--mCnt;
//			return true;
//		}
//	}
//
//	bool PolyLine::TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const
//	{
//		if (mCnt <= 0)
//		{
//			return false;
//		}
//
//		float minX = 100;
//		float maxX = 0;
//
//		float minY = 100;
//		float maxY = 0;
//		
//		
//		for (int i = 0; i < mCnt; ++i)
//		{
//			if (minX > mP[i].GetX())
//			{
//				minX = mP[i].GetX();
//			}
//
//			if (minY > mP[i].GetY())
//			{
//				minY = mP[i].GetY();
//			}
//
//			if (maxX < mP[i].GetX())
//			{
//				maxX = mP[i].GetX();
//			}
//
//			if (maxY < mP[i].GetY())
//			{
//				maxY = mP[i].GetY();
//			}
//		}
//
//		Point min(minX, minY);
//		Point max(maxX, maxY);
//
//		*outMin = min;
//		*outMax = max;
//
//		return true;
//	}
//
//	const Point* PolyLine::operator[](unsigned int i) const
//	{
//		int index = static_cast<int>(i);
//
//		if (mCnt < index)
//		{
//			return NULL;
//		}
//		
//		float x, y;
//
//		x = mP[index].GetX();
//		y = mP[index].GetY();
//
//		return new Point(x, y);
//		return new Point(0, 0);
//	}
//
//	//PolyLine& PolyLine::operator=(const PolyLine& other)
//	//{
//	//	mP = other.mP;
//	//	return *this;
//	//}
//
//	void PolyLine::ShowPoint()
//	{
//		for (int i = 0; i < mCnt; ++i)
//		{
//			std::cout << "-----------------------" << std::endl;
//			std::cout << i << std::endl;
//			std::cout << mP[i].GetX() << ", " << mP[i].GetY() << std::endl;
//			std::cout << "-----------------------" << std::endl;
//		}
//	}
//}
//
//
