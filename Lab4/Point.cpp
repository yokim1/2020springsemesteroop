#include "Point.h"
#include <iostream>

using namespace std;

namespace lab4
{
	Point::Point()
		: mX{ '\0' }, mY{ '\0' }
	{ 
		cout << this << "Point()" << endl;
	}

	Point::Point(float x, float y)
		: mX{ x }, mY{ y }
	{
		cout << this << "Point(float x, float y)" << endl;
	}

	Point::~Point()
	{
		cout << this << "~Point()" << endl;
		//delete mPointAddr;
		//delete[] mPolyArrAddr;
	}

	Point Point::operator+(const Point& other) const
	{
		return Point(GetX() + other.GetX(), GetY() + other.GetY());
	}

	Point Point::operator-(const Point& other) const
	{
		return Point(GetX() - other.GetX(), GetY() - other.GetY());
	}

	float Point::Dot(const Point& other) const
	{
		return GetX() * other.GetX() + GetY() * other.GetY();
	}

	Point Point::operator*(float operand) const
	{
		return Point(GetX() * operand, GetY() * operand);
	}

	Point operator*(float lhs, const Point& rhs)
	{
		return Point(rhs.GetX() * lhs, rhs.GetY() * lhs);
	}

	float Point::GetX() const
	{
		return mX;
	}

	float Point::GetY() const
	{
		return mY;
	}
}
