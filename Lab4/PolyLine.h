#pragma once
#include "Point.h"

namespace lab4
{
	class PolyLine : public Point
	{
	public:
		PolyLine();
		PolyLine(const PolyLine& other);
		~PolyLine();

		bool AddPoint(float x, float y);
		bool AddPoint(const Point* point);
		bool RemovePoint(unsigned int i);
		bool TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const;

		const Point* operator[](unsigned int i) const;
		PolyLine& operator=(const PolyLine& rhs);

	private:
		//Point* mPolyLine;
		const Point** mPolyArrAddr;//85점이후 이것을 추가
		int mCnt;
		int mCntLimit = 10;
	};
}