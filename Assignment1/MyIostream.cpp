#include "MyIostream.h"

int Mystrlen(const char* s) 
{
	int i;
	for (i = 0; ; ++i)
	{
		if (s[i] == '\0' || s[i] == 0) 
		{
			break;
		}
	}
	return i;
}

void Mymemcpy(char* dest, const char* src, const int len) 
{
	for (int i = 0; i < len; ++i)
	{
		dest[i] = src[i];
	}
}

void Myconcat(char* s1, const char* s2)
{
	int len1 = Mystrlen(s1);
	int len2 = Mystrlen(s2);
	int totalLen = len1 + len2 + 1;
	int j = 0;
	
	for (int i = len1; i < totalLen; ++i)
	{
		s1[i] = s2[j];
		++j;
	}
}

void MySwap(char& s1, char& s2)
{
	char tmp;

	tmp = s1;
	s1 = s2;
	s2 = tmp;
}