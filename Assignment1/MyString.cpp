#include "MyString.h"

using namespace std;

//IndexOf, LastIndexOf, InterLeave(이것 때문에 timeout 발생) 남음

namespace assignment1
{
	MyString::MyString() : mLen { 0 }, mP{nullptr}
	{
		//default constructor
	}

	//constructor
	MyString::MyString(const char* s) 
		: mLen { Mystrlen(s)}, mP{new char[mLen + 1]}
	{
		//mLen = Mystrlen(s);
		//mP = new char[mLen];
		Mymemcpy(mP, s, mLen + 1);
	}

	//copy constructor
	MyString::MyString(const MyString& other) 
		: mLen{other.mLen}, mP{new char[mLen + 1]}
	{
		Mymemcpy(mP, other.mP, mLen + 1);
	}

	MyString::~MyString()
	{

		delete[] mP;

	}

	unsigned int MyString::GetLength() const
	{
		return mLen;
	}

	const char* MyString::GetCString() const
	{
		//콘솔에서 뒤에 이상한 글자가 나온다.
		//해결 생각: 뒤에 글자를 어떤 값으로 초기화를 해준다.
		return mP;
	}

	void MyString::Append(const char* s)
	{
		int sLen = Mystrlen(s);
		
		if (sLen == 0)
		{
			return;
		}

		int appendStringLen = mLen + sLen;
		
		char* appendString = new char[appendStringLen + 1];

		Mymemcpy(appendString, mP, mLen + 1);
		delete[] mP;
		Myconcat(appendString, s);

		mLen = appendStringLen;
		mP = appendString;
	}

	MyString MyString::operator+(const MyString& other) const
	{
		//MyString res;
		//res.mLen = mLen + other.mLen;
		//res.mP = new char[res.mLen + 1];

		//Mymemcpy(res.mP, mP, mLen + 1);
		//Mymemcpy(res.mP + mLen, other.mP, other.mLen + 1);
		////delete[]mP;
		//return res;

		char* res = new char[Mystrlen(mP) + Mystrlen(other.mP) + 1];
		Mymemcpy(res, mP, mLen + 1);
		Myconcat(res, other.mP);
		MyString finalRes = MyString(res);

		delete[]res;
		return finalRes;
	}

	int MyString::IndexOf(const char* s)
	{
		//<LOGIC>
		//들어온 것의 글자수를 count한다.
		//- 글자수가 없다면, return 0
		//- 글자수가 있다면, 다음단계


		//-글자가 여러개일때
		//s의 첫번째 글자와 기존의 첫번째 글자와 매치하는 것을 찾는다.
		//이때 찾을때 position을 카운트해준다.
		//이후에도 s의 글자 수 만큼 매치를 하는지 검사한다.
		// - 매치한다면 return position - 1;
		// - 매치하지 않으면 return -1;

		//-글자가 한개일때
		//s의 글자와 기존의 문자열과 매치하는 것을 찾는다.
		//이때 찾을때 position을 카운트해준다.
		// - 찾았으면 return position;
		// - 못 찾았으면 return -1;

		int sLen = Mystrlen(s);
		
		if (sLen == 0)
		{
			return 0;
		}

		int cnt = 0;
		int notFounded = -1;

		for (int mIndex = 0; mIndex < mLen; ++mIndex)
		{
			if (mP[mIndex] == s[0])
			{
				cnt = 0;
				for (int sIndex = 0; sIndex < sLen; ++sIndex) 
				{
					if (mP[mIndex + sIndex] == s[sIndex])
					{
						++cnt;
					}
					else 
					{
						break;
					}
				}
			}

			if (cnt == sLen)
			{
				return mIndex;
			}
		}

		return notFounded;
	}

	int MyString::LastIndexOf(const char* s)
	{
		//<LOGIC>
		//들어온 것의 글자수를 count한다.
		//- 글자수가 없다면, return 0
		//- 글자수가 있다면, 다음단계


		//-글자가 여러개일때
		//s의 첫번째 글자와 기존의 첫번째 글자와 매치하는 것을 찾는다.
		//이때 뒤에서 부터 찾는다. 그리고 찾을때 position을 카운트해준다.
		//이후에도 s의 글자 수 만큼 매치를 하는지 검사한다.
		// - 매치한다면 return position - 1;
		// - 매치하지 않으면 return -1;

		//-글자가 한개일때
		//s의 글자와 기존의 문자열과 매치하는 것을 찾는다.
		//이때 찾을때 position을 카운트해준다.
		// - 찾았으면 return position;
		// - 못 찾았으면 return -1;

		int sLen = Mystrlen(s);

		if (sLen == 0)
		{
			return mLen;
		}

		int cnt = 0;
		int notFounded = -1;
		int checkmIndex = 0;
		for (int mIndex = mLen - 1; mIndex >= 0; --mIndex)
		{
			checkmIndex = mIndex;
			cnt = 0;
			for (int sIndex = sLen - 1; sIndex >= 0; --sIndex)
			{
				if (mP[checkmIndex] == s[sIndex])
				{
					++cnt;
					--checkmIndex;
				}
				else
				{
					break;
				}
			}
			if (cnt == sLen)
			{
				return checkmIndex + 1;
			}
		}
		return notFounded;
	}

	void MyString::Interleave(const char* s)
	{ 
		//이것 때문에 time out이 발생

		//1.길이에 맞는 보관소를 준비
		//	- mP와 s와의 길이가 얼마나 다른지 검사.
		//	- 끝에 남는 문자들을 마지막에는 다 붙여줘야 한다.
		//2.그 보관소 짝수에는 원래있던 글자들을 삽입
		//3.그 보관소 홀수에는 새로 들어온 글자들을 삽입
		//4.그 보관소의 주소를 MyString으로 업데이트
		//5.그 보관소의 길이를 MyString으로 업데이트

		//새로 들어온 글자들의 수를 파악
		//0이면 그냥 return
		
		int sLen = Mystrlen(s);

		if (sLen == 0)
		{
			return;
		}

		int interLeaveLen = mLen + sLen;
		char* interLeaveStr = new char[interLeaveLen + 1];

		int mIndex = 0;
		int sIndex = 0;
		int tmpIndex;

		for (int i = 0; i < interLeaveLen; ++i)
		{
			if (i % 2 == 0)
			{
				if (mIndex == mLen)
				{
					tmpIndex = i;
					break;
				}
				interLeaveStr[i] = mP[mIndex];
				++mIndex;
			}
			else if (i % 2 == 1)
			{
				if (sIndex == sLen)
				{
					tmpIndex = i;
					break;
				}
				interLeaveStr[i] = s[sIndex];
				++sIndex;
			}
		}

		if (mLen < sLen)
		{
			for (int i = tmpIndex; i < interLeaveLen; ++i)
			{
				interLeaveStr[i] = s[sIndex];
				++sIndex;
			}
		}
		else if (sLen < mLen)
		{
			for (int i = tmpIndex; i < interLeaveLen; ++i)
			{
				interLeaveStr[i] = mP[mIndex];
				++mIndex;
			}
		}

		interLeaveStr[interLeaveLen] = '\0';

		delete[] mP;

		mP = interLeaveStr;
		mLen = interLeaveLen;
	}

	bool MyString::RemoveAt(unsigned int index)
	{
		//<LOGIC>
		//문자열을 count해서, 해당 인덱스 번호를 찾는다.
		//- 찾으면: true; 지워준다.
		//- 없으면: false; 그냥 돌아온다.

		//- 찾으면(Detail ver.):
		//기존보다 하나를 줄인 보관소를 준비한다. 
		//해당 인덱스에 위치한 mP[]만을 제외한 것들을 넣는다.
		//새로운 보관소의 주소를 MyString에 업데이트
		//새로운 보관소의 길이를 MyString에 업데이트

		int rIndex = 0;
		int idx = static_cast<int>(index);

		if (mLen <= idx)
		{
			return false;
		}

		if (idx < mLen)
		{
			char* removeAtString = new char[mLen];

			for (int i = 0; i < mLen + 1; ++i)
			{
				if (i == idx)
				{
					continue;
				}
				else
				{
					removeAtString[rIndex] = mP[i];
					++rIndex;
				}
			}
			delete[] mP;
			mP = removeAtString;
			mLen = mLen - 1;
			return true;
		}
		else 
		{
			return false;
		}

	}

	void MyString::PadLeft(unsigned int totalLength)
	{
		//<LOGIC>
		//MyString에 문자열 길이와 totalLength의 길이를 비교
		//- 같거나 적으면; 변화 x
		//- 더 크다면(초과); mLen - totalLength 만큼 글 앞에 빈 공간을 만들어 준다.
		
		//- 더 크다면(detail):
		//totalLength만큼 새로운 보관소를 만든다.
		//기존에 있던 문자 수 만큼은 제외하고 (mLen - totalLength)
		//새로 들어온 기호를 넣는다.
		//새로운 보관소의 주소를 mP로 업데이트
		//새로운 보관소의 길이를 mLen로 업데이트

		PadLeft(totalLength, ' ');
	}

	void MyString::PadLeft(unsigned int totalLength, const char c)
	{
		
		int remainLen;
		int ttLen = static_cast<int>(totalLength);
		
		if (ttLen <= mLen)
		{
			return;
		}
		//- 더 크다면(detail):
		//totalLength만큼 새로운 보관소를 만든다.
		//기존에 있던 문자 수 만큼은 제외하고 (totalLength - mLen)
		//새로 들어온 기호를 넣는다.
		//새로운 보관소의 주소를 mP로 업데이트
		//새로운 보관소의 길이를 mLen로 업데이트
		else
		{
			char* padLeftString = new char[totalLength + 1];

			remainLen = ttLen - mLen;
			int i = 0;
			int mIndex = 0;
			
			for (i = 0; i < remainLen; ++i)
			{
				padLeftString[i] = c;
			}
			for (i = remainLen; i < ttLen + 1; ++i)
			{
				padLeftString[i] = mP[mIndex];
				++mIndex;
			}

			delete[] mP;
			
			mP = padLeftString;
			mLen = ttLen;
		}
	}

	void MyString::PadRight(unsigned int totalLength)
	{
		PadRight(totalLength, ' ');
	}

	void MyString::PadRight(unsigned int totalLength, const char c)
	{
		//<LOGIC>
		//MyString에 문자열 길이와 totalLength의 길이를 비교
		//- 같거나 적으면; 변화 x
		//- 더 크다면(초과); mLen - totalLength 만큼 글 뒤에 빈 공간을 만들어 준다.

		//- 더 크다면(detail):
		//totalLength만큼 새로운 보관소를 만든다.
		//기존에 있던 문자를 새로운 보관소 앞 부분 부터 채운다.
		//채워진것의 맨 끝부터 새로운 문자로 채운다.
		//새로운 보관소의 주소를 mP로 업데이트
		//새로운 보관소의 길이를 mLen로 업데이트

		
		int remainLen;
		int ttLen = static_cast<int>(totalLength);

		if (ttLen <= mLen)
		{
			return;
		}
		else
		{
			char* padRightString = new char[totalLength + 1];

			remainLen = ttLen - mLen;
			int i = 0;
			int mIndex = 0;

			for (i = 0; i < mLen + 1; ++i)
			{
				padRightString[i] = mP[mIndex];
				++mIndex;
			}
			for (i = mLen; i < ttLen + 1; ++i)
			{
				padRightString[i] = c;
			}

			delete[] mP;

			padRightString[ttLen] = '\0';

			mP = padRightString;
			mLen = ttLen;
		}
	}

	void MyString::Reverse()
	{
		//<LOGIC>
		//tmp를 만들어서 임시 보관소로 사용.
		int endOfIndx = mLen - 1;

		for (int i = 0; i < mLen / 2; ++i)
		{
			MySwap(mP[i], mP[endOfIndx]);
			--endOfIndx;
		}
	}

	bool MyString::operator==(const MyString& rhs) const
	{
		//<LOGIC>
		//mLen과 rhs의 길이를 비교해준다.
		//- 같다면, 다음단계
		//- 다르다면, false

		//길이가 같은 문자열이라면, 글자의 한글자 한글자가 같은지 본다.
		//- 한글자 한글자 모두 같다면, true
		//- 한글자라도 다르다면, false

		if (mLen == rhs.mLen)
		{
			for (int i = 0; i < mLen; ++i)
			{
				if (mP[i] != rhs.mP[i])
				{
					return false;
				}
			}
			return true;
		}
		else if (mLen != rhs.mLen)
		{
			return false;
		}
		return false;
	}

	MyString& MyString::operator=(const MyString& rhs)
	{
		//<LOGIC>
		//기존의 mP에 rhs의 값을 넣기 위해, 메모리 해재
		//기존의 mLen을 rhs.mLen으로 업데이트
		//mLen+1길이 만큼 mP에 할당을 해준다.
		//그 mP에 rhs의 mP를 Mymemcpy를 이용해서 넣는다.

		if (*this == rhs)
		{
			return *this;
		}

		delete[] mP;


		mLen = rhs.mLen;

		mP = new char[mLen + 1];
		Mymemcpy(mP, rhs.mP, mLen + 1);

		return *this;
	}

	void MyString::ToLower()
	{
		//<LOGIC>
		//소문자로 바꿀수 있는 것인지 아닌것인지 글자 하나하나를 검사.
		//- 소문자로 바꿀수 있는 것이 아니라면, 그대로 둔다.
		//- 소문자로 바꿀수 있는 것이라면, 바꾼다.

		//ACII 32+- 하면 대,소 문자로 바꿀수 있다.

		for (int i = 0; i < mLen; ++i)
		{
			if ('A' <= mP[i] && mP[i] <= 'Z')
			{
				mP[i] = mP[i] + 32;
			}
		}
	}

	void MyString::ToUpper()
	{
		for (int i = 0; i < mLen; ++i)
		{
			if ('a' <= mP[i] && mP[i] <= 'z')
			{
				mP[i] = mP[i] - 32;
			}
		}
	}
	

}