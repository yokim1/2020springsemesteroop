#pragma once

int Mystrlen(const char* s);
void Mymemcpy(char* dest, const char* src, const int len);
void Myconcat(char* dest, const char* src);
void MySwap(char& s1, char& s2);