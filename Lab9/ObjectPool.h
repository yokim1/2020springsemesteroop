#pragma once

#include <queue>
#include <array>

using namespace std;

namespace lab9
{
	template<class T>
	class ObjectPool
	{
	public:
		ObjectPool(size_t maxPoolSize);
		ObjectPool(const ObjectPool& other) = delete;
		ObjectPool<T>& operator=(const ObjectPool& rhs) = delete;
		~ObjectPool();
		
		T* Get();
		void Return(T*);
		size_t GetFreeObjectCount();
		size_t GetMaxFreeObjectCount();

	private:
		queue<T*> mObjectPool;
		size_t mMaxPoolSize;
		size_t mFreeObjectCount;
	};

	template<class T>
	inline ObjectPool<T>::ObjectPool(size_t maxPoolSize)
		: mMaxPoolSize(maxPoolSize), mFreeObjectCount(0)
	{

	}
	
	template<class T>
	inline ObjectPool<T>::~ObjectPool()
	{
		while (true)
		{
			if (mObjectPool.size() == 0)
			{
				break;
			}

			T* deleteObject = mObjectPool.front();
			mObjectPool.pop();
			delete deleteObject;
		}
	}

	template<class T>
	inline T* ObjectPool<T>::Get()
	{
		if (mObjectPool.size() != 0)
		{	
			T* mObjectPoolFrontPtr = mObjectPool.front();
			mObjectPool.pop();

			return mObjectPoolFrontPtr;
		}
		else
		{
			return new T();
		}
	}

	template<class T>
	inline void ObjectPool<T>::Return(T* returnPtr)
	{
		if (mObjectPool.size() != mMaxPoolSize)
		{
			mObjectPool.push(returnPtr);
			++mFreeObjectCount;
			return;
		}
		else
		{
			delete returnPtr;
		}
	}

	template<class T>
	inline size_t ObjectPool<T>::GetFreeObjectCount()
	{
		//return mFreeObjectCount;
		return static_cast<size_t>(mObjectPool.size());
	}

	template<class T>
	inline size_t ObjectPool<T>::GetMaxFreeObjectCount()
	{
		return mMaxPoolSize;
	}
}