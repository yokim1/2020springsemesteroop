#include "Sedan.h"

namespace assignment2
{
	Sedan::Sedan()
		: Vehicle(4)
	{
		this->mTravelLimit = 5;

		if (mbTrailerCheck == true)
		{
			this->mRestLimit = 2;
		}
		else if (mbTrailerCheck == false)
		{
			this->mRestLimit = 1;
		}

		mTrailer = nullptr;

	}

	Sedan::~Sedan()
	{
		delete mTrailer;
		mTrailer = nullptr;
	}

	//Sedan::Sedan(const Sedan& other)
	//	: Vehicle(other)
	//{
	//	//Shallow copy
	//	//mTrailer = other.mTrailer;

	//	//Deep copy
	//	this->mbRestCheck = other.mbRestCheck;

	//	if (this->mbTrailerCheck == true)
	//	{
	//		mTrailer = new Trailer(other.mTrailer->GetWeight());

	//		this->mRestLimit = 2;
	//	}
	//	else
	//	{
	//		this->mRestLimit = 1;
	//	}

	//	this->mbTrailerCheck = other.mbTrailerCheck;
	//	this->mbTravelCheck = other.mbTravelCheck;
	//}

	//Sedan& Sedan::operator=(const Sedan& rhs)
	//{
	//	if (this == &rhs)
	//	{
	//		return *this;
	//	}
	//	else
	//	{
	//		Vehicle::operator=(rhs);
	//		
	//		delete mTrailer;
	//		mTrailer = nullptr;
	//		this->mbTrailerCheck = rhs.mbTrailerCheck;

	//		if (this->mbTrailerCheck == true && rhs.mTrailer != nullptr)
	//		{
	//			mTrailer = new Trailer(rhs.mTrailer->GetWeight());

	//			this->mRestLimit = 2;
	//		}
	//		else
	//		{
	//			this->mRestLimit = 1;
	//		}
	//		
	//		this->mbTravelCheck = rhs.mbTravelCheck;
	//		this->mbRestCheck = rhs.mbRestCheck;

	//		return *this;
	//	}
	//}

	unsigned int Sedan::GetMaxSpeed() const
	{
		return GetDriveSpeed();
	}

	unsigned int Sedan::GetDriveSpeed() const
	{
		/*If x <= 80, speed = 480
			If x > 80, speed = 458
			If x > 160, speed = 400
			If x > 260, speed = 380
			If x > 350, speed = 300*/
		unsigned int totalWeight;

		if (mbTrailerCheck == true || mTrailer != nullptr)
		{
			totalWeight = mTrailer->GetWeight() + this->GetPassengersTotalWeight();
		}
		else
		{
			totalWeight = this->GetPassengersTotalWeight();
		}

		if (totalWeight > 350)
		{
			return 300;
		}
		else if (totalWeight > 260)
		{
			return 380;
		}
		else if (totalWeight > 160)
		{
			return 400;
		}
		else if (totalWeight >	80)
		{
			return 458;
		}
		else if (totalWeight <= 80)
		{
			return 480;
		}

		return 0;
	}

	bool Sedan::AddTrailer(const Trailer* trailer)
	{
		if (mbTrailerCheck == true || mTrailer != nullptr)
		{
			this->mRestLimit = 2;
			return false;
		}
		
		mTrailer = trailer;
		mbTrailerCheck = true;
		this->mRestLimit = 2;

		return true;
	}

	bool Sedan::RemoveTrailer()
	{
		if (mbTrailerCheck == false || mTrailer == nullptr)
		{
			this->mRestLimit = 1;
			return false;
		}

		delete mTrailer;
		mTrailer = nullptr;
		mbTrailerCheck = false;
		this->mRestLimit = 1;

		return true;

	}
}