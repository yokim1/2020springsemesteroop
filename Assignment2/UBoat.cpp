#include "UBoat.h"

namespace assignment2
{
	UBoat::UBoat()
		: Vehicle(50)
	{
		this->mTravelLimit = 2;
		this->mRestLimit = 4;
	}

	UBoat::~UBoat()
	{
	}

	unsigned int UBoat::GetMaxSpeed() const
	{
		unsigned int sailSpeed = this->GetSailSpeed();
		unsigned int diveSpeed = this->GetDiveSpeed();

		if (sailSpeed > diveSpeed)
		{
			return sailSpeed;
		}
		else
		{
			return diveSpeed;
		}
	}

	unsigned int UBoat::GetSailSpeed() const
	{
	
		unsigned int totalWeight = this->GetPassengersTotalWeight();
		double rhs;
		rhs = (550.0 - (totalWeight / 10.0));
		unsigned int lhs = 200;
		
		if (rhs > lhs)
		{
			return static_cast<unsigned int>(round(rhs));
		}
		else
		{
			return lhs;
		}
	}

	unsigned int UBoat::GetDiveSpeed() const
	{
		unsigned int totalWeight = this->GetPassengersTotalWeight();
		unsigned int result;

		result = static_cast<unsigned int>(round(500.0 * log((totalWeight + 150.0) / 150.0) + 30.0));
		return result;
	}

}