#pragma once

#include "Vehicle.h"

namespace assignment2
{
	class DeusExMachina
	{
	public:
		DeusExMachina();
		~DeusExMachina();

		static DeusExMachina* GetInstance();
		void Travel() const;
		bool AddVehicle(Vehicle* vehicle);
		bool RemoveVehicle(unsigned int i);
		const Vehicle* GetFurthestTravelled();
		const Vehicle* GetVehicle(unsigned int i) const;
		//unsigned int GetTravelDistance();
		Vehicle* GetVehicle(unsigned int);
		unsigned int GetCnt() const;

	private:
		
		Vehicle** mTakeControl;
		unsigned int mControlCnt;
		const unsigned int MAX_CONTROL = 10;
		unsigned int mIndexOfFurthestTravelledVehicle;

		
	};
}