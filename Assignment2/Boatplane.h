#pragma once
#include "Vehicle.h"
#include "IFlyable.h"
#include "ISailable.h"
#include <math.h>

namespace assignment2
{
	class Boatplane : public Vehicle, public IFlyable, public ISailable
	{
	public:
		Boatplane(unsigned int maxPassengersCount);
		Boatplane(unsigned int, unsigned int, unsigned int, const Person**, const Person**);
		~Boatplane();

		unsigned int GetMaxSpeed() const;
		unsigned int GetFlySpeed() const;
		unsigned int GetSailSpeed() const;
	};
}