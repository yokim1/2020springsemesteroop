#include "Boat.h"
#include "Airplane.h"
#include <math.h>

namespace assignment2
{
	Boat::Boat(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		this->mTravelLimit = 2;
		this->mRestLimit = 1;
	}

	Boat::~Boat()
	{

	}

	unsigned int Boat::GetMaxSpeed()const
	{
		return this->GetSailSpeed();
	}
	
	unsigned int Boat::GetSailSpeed() const
	{
		double totalWeight = static_cast<double>(this->GetPassengersTotalWeight());
		double result;

		result = 800.0 - 10.0 * totalWeight;

		if (result > 20)
		{
			return static_cast<unsigned int>(round(result));
		}
		else
		{
			return 20;
		}
	}


	Boatplane Boat::operator+(Airplane& plane)
	{
		unsigned int boatPassengersNumber = this->GetPassengersCount();
		unsigned int planePassengersNumber = plane.GetPassengersCount();
		unsigned int totalMaxPassengers = this->GetMaxPassengersCount() + plane.GetMaxPassengersCount();

		this->SetPassengersZero();
		plane.SetPassengersZero();

		return Boatplane(totalMaxPassengers, planePassengersNumber, boatPassengersNumber, plane.GetPassengers(), this->GetPassengers());
	}
}