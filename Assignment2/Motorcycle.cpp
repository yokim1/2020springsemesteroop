#include "Motorcycle.h"

namespace assignment2
{
	Motorcycle::Motorcycle()
		: Vehicle(2)
	{
		this->mTravelLimit = 5;
		this->mRestLimit = 1;
	}

	Motorcycle::~Motorcycle()
	{
	}

	unsigned int Motorcycle::GetMaxSpeed() const
	{
		return this->GetDriveSpeed();
	}

	unsigned int Motorcycle::GetDriveSpeed() const
	{
		unsigned int totalWeight = (this->GetPassengersTotalWeight());
		double lhs; 
		lhs = (400.0 + 2.0 * totalWeight - pow(totalWeight / 15, 3));
		unsigned int rhs = 0;

		
		if (lhs > rhs)
		{
			return static_cast<unsigned int >(round(lhs));
		}
		else
		{
			return rhs;
		}

	}
}