#pragma once
#include "Vehicle.h"
#include "IDrivable.h"
#include "Trailer.h"

namespace assignment2
{
	class Trailer;

	class Sedan : public Vehicle, public IDrivable
	{
	public:
		Sedan();
		~Sedan();

		//Sedan(const Sedan&);
		//Sedan& operator=(const Sedan& rhs);

		unsigned int GetMaxSpeed() const;
		unsigned int GetDriveSpeed() const;

		bool AddTrailer(const Trailer* trailer);
		bool RemoveTrailer();

	private:
		const Trailer* mTrailer;
		unsigned int mMaxPassengers = 4;
		bool mbTrailerCheck;

	};
}