#include "DeusExMachina.h"

namespace assignment2
{
	DeusExMachina::DeusExMachina()
	{
		mTakeControl = new Vehicle* [MAX_CONTROL];
		mControlCnt = 0;
		for (unsigned int i = 0; i < MAX_CONTROL; ++i)
		{
			mTakeControl[i] = nullptr;
		}
	}
	DeusExMachina::~DeusExMachina()
	{
		for (unsigned int i = 0; i < MAX_CONTROL; ++i)
		{
			if (mTakeControl[i] != nullptr) 
			{
				delete mTakeControl[i];
			}
			mTakeControl[i] = nullptr;
		}
		delete mTakeControl;
	}

	DeusExMachina* DeusExMachina::GetInstance()
	{
		static DeusExMachina oneOfKind;
		return &oneOfKind;
	}

	void DeusExMachina::Travel() const
	{
		for (unsigned int i = 0; i < mControlCnt; ++i)
		{
			mTakeControl[i]->IncreaseTravelCount();
		}
	}

	bool DeusExMachina::AddVehicle(Vehicle* vehicle)
	{
		if (mControlCnt == MAX_CONTROL || vehicle == nullptr)
		{
			return false;
		}
		else
		{
			mTakeControl[mControlCnt] = vehicle;
			++mControlCnt;
			return true;
		}
	}

	bool DeusExMachina::RemoveVehicle(unsigned int i)
	{
		if (MAX_CONTROL <= i || mControlCnt <= i)
		{
			return false;
		}
		else
		{
			delete mTakeControl[i];

			for (unsigned int index = i; index < mControlCnt - 1; ++index)
			{
				mTakeControl[index] = mTakeControl[index + 1];
			}
			for (unsigned int index = mControlCnt - 1; index < MAX_CONTROL; ++index)
			{
				mTakeControl[index] = nullptr;
			}

			--mControlCnt;
			return true;
		}
	}


	const Vehicle* DeusExMachina::GetFurthestTravelled()
	{
		//운송 수단이 없으면, NULL
		//움직인 운송수단이 없으면, 첫번째 반환

		if (mControlCnt == 0)
		{
			return nullptr;
		}

		/*unsigned int maxTravelledDistance = 0;
		unsigned int travelledDistance = 0;
		unsigned int maxTravelledIndex = 0;*/

		unsigned int max = mTakeControl[0]->GetTravelDistance();
		unsigned int furthestTravelledVehicle;


		//for (unsigned int i = 0; i < mControlCnt; ++i)
		//{
		//	travelledDistance = mTakeControl[i]->GetTravelDistance();

		//	if (maxTravelledDistance < travelledDistance)
		//	{
		//		maxTravelledDistance = travelledDistance;
		//		maxTravelledIndex = i;
		//	}
		//}
		//
		//if (maxTravelledDistance == 0 && travelledDistance == 0)
		//{
		//	return mTakeControl[0];
		//}

		//return mTakeControl[maxTravelledIndex];


		unsigned int index;
		for (index = 1; index < mControlCnt; ++index)
		{
			if (max < mTakeControl[index]->GetTravelDistance())
			{
				max = mTakeControl[index]->GetTravelDistance();
				furthestTravelledVehicle = index;
			}
		}

		if (max == 0)
		{
			return mTakeControl[0];
		}

		mIndexOfFurthestTravelledVehicle = furthestTravelledVehicle;

		return mTakeControl[mIndexOfFurthestTravelledVehicle];
	}

	const Vehicle* DeusExMachina::GetVehicle(unsigned int i) const
	{
		if (MAX_CONTROL <= i)
		{
			return NULL;
		}
		else
		{
			return mTakeControl[i];
		}
	}

	Vehicle* DeusExMachina::GetVehicle(unsigned int i)
	{
		return mTakeControl[i];
	}

	unsigned int DeusExMachina::GetCnt() const
	{
		return mControlCnt;
	}
}