#include "Boatplane.h"
#include "Airplane.h"

namespace assignment2
{
	Boatplane::Boatplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		this->mTravelLimit = 1;
		this->mRestLimit = 3;
	}

	Boatplane::Boatplane(unsigned int maxPassengersCount, unsigned int mSeats1PassengersCount, unsigned int mSeats2PassengersCount, const Person** mSeats1, const Person** mSeats2)
		: Vehicle(maxPassengersCount, mSeats1PassengersCount, mSeats2PassengersCount, mSeats1, mSeats2)
	{
		this->mTravelLimit = 1;
		this->mRestLimit = 3;
	}

	Boatplane::~Boatplane()
	{
	}

	unsigned int Boatplane::GetMaxSpeed() const
	{
		unsigned int flySpeed = (this->GetFlySpeed());
		unsigned int sailSpeed = (this->GetSailSpeed());

		if (flySpeed > sailSpeed)
		{
			return flySpeed;
		}
		else
		{
			return sailSpeed;
		}
		return flySpeed;
	}

	unsigned int Boatplane::GetFlySpeed() const
	{
		unsigned int totalWeight = (this->GetPassengersTotalWeight());
		double result;

		result = 150.0 * exp((500.0 - totalWeight) / 300.0);
		return static_cast<unsigned int>(round(result));
	}

	unsigned int Boatplane::GetSailSpeed() const
	{
		unsigned int totalWeight = this->GetPassengersTotalWeight();
		double result = (800.0 - 1.7 * totalWeight);

		if (result > 20)
		{
			return static_cast<unsigned int>(round(result));
		}
		else
		{
			return 20;
		}
	}
}