#pragma once
#include "Person.h"

namespace assignment2
{
	class Vehicle
	{
	public:
		//Vehicle();
		Vehicle(unsigned int maxPassengersCount);
		Vehicle(unsigned int, unsigned int, unsigned int, const Person**, const Person**);
		Vehicle(const Vehicle&);
		Vehicle& operator=(const Vehicle& rhs);
		
		~Vehicle();

		virtual unsigned int GetMaxSpeed() const = 0;

		bool AddPassenger(const Person* person);
		bool RemovePassenger(unsigned int i);
		const Person** GetPassengers() const;
		const Person* GetPassenger(unsigned int i) const;
		unsigned int GetPassengersCount() const;
		unsigned int GetMaxPassengersCount() const;
		unsigned int GetPassengersTotalWeight() const;
		
		void SetPassengersZero();

		void IncreaseTravelCount();
		

		unsigned int GetTravelDistance();

		
		unsigned int GetTotalTravelCnt() const;
		unsigned int GetTravelCnt() const;
		unsigned int GetTravelLimit() const;
		unsigned int GetRestCnt() const;
		unsigned int GetRestLimit() const;


	protected:
		unsigned int mTotalTravelCnt;
		unsigned int mRestCnt;
		unsigned int mRestLimit;
		unsigned int mTravelCnt;
		unsigned int mTravelLimit;


		bool mbRestCheck = false;
		bool mbTravelCheck = true;

	private:
		const Person** mSeats;
		unsigned int mMaxPassengersCount;
		unsigned int mPassengersCnter = 0;
	};
	
}