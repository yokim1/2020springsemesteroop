#include "Vehicle.h"

namespace assignment2
{

	Vehicle::Vehicle(unsigned int maxPassengersCount)
		: mMaxPassengersCount(maxPassengersCount), mPassengersCnter(0), mTravelLimit(0), mTravelCnt(0), mRestLimit(0), mRestCnt(0)
	{
		mSeats = new const Person*[maxPassengersCount];
		for (unsigned int i = 0; i < mMaxPassengersCount; ++i)
		{
			mSeats[i] = nullptr;
		}
	}
	Vehicle::Vehicle(unsigned int maxPassengersCount, unsigned int seats1PassengersCount, unsigned int seats2PassengersCount, const Person** mSeats1, const Person** mSeats2)
		: mMaxPassengersCount(maxPassengersCount), mPassengersCnter(0), mTravelLimit(0), mTravelCnt(0), mRestLimit(0), mRestCnt(0)
	{
		mSeats = new const Person*[mMaxPassengersCount];

		for (unsigned int i = 0; i < mMaxPassengersCount; ++i)
		{
			mSeats[i] = nullptr;
		}

		for (unsigned int i = 0; i < seats1PassengersCount; ++i)
		{
			AddPassenger(mSeats1[i]);
			mSeats1[i] = nullptr;
		}

		int index = 0;
		for (unsigned int i = seats1PassengersCount; i < seats1PassengersCount + seats2PassengersCount; ++i)
		{
			AddPassenger(mSeats2[index]);
			mSeats2[index] = nullptr;
			++index;
		}
	}

	Vehicle::Vehicle(const Vehicle& other)
		: mMaxPassengersCount(other.mMaxPassengersCount), mPassengersCnter(other.mPassengersCnter), mTravelLimit(other.mTravelLimit), mTravelCnt(other.mTravelCnt), mRestLimit(other.mRestLimit), mRestCnt(other.mRestCnt)
	{
		mSeats = new const Person*[mMaxPassengersCount];

		for (unsigned int i = 0; i < mMaxPassengersCount; ++i)
		{
			mSeats[i] = nullptr;
		}

		for (unsigned int i = 0; i < mPassengersCnter; ++i)
		{
			mSeats[i] = new const Person(other.mSeats[i]->GetName().c_str(), other.mSeats[i]->GetWeight());
		}

	}

	Vehicle& Vehicle::operator=(const Vehicle& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		if (mPassengersCnter != 0)
		{
			delete[] mSeats;
		}

		mMaxPassengersCount = rhs.mMaxPassengersCount;
		mPassengersCnter = rhs.mPassengersCnter;
		mTravelCnt = rhs.mTravelCnt;
		mTravelLimit = rhs.mTravelLimit;
		mRestCnt = rhs.mRestCnt;
		mRestLimit = rhs.mRestLimit;

		mSeats = new const Person*[mMaxPassengersCount];

		for (unsigned int i = 0; i < mMaxPassengersCount; ++i)
		{
			mSeats[i] = nullptr;
		}

		for (unsigned int i = 0; i < mPassengersCnter; ++i)
		{
			mSeats[i] = new const Person(rhs.mSeats[i]->GetName().c_str(), rhs.mSeats[i]->GetWeight());
		}

		return *this;

	}

	Vehicle::~Vehicle()
	{
		for (unsigned int i = 0; i < mMaxPassengersCount; ++i)
		{
			delete mSeats[i];
		}

		delete[] mSeats;
	}

	bool Vehicle::AddPassenger(const Person* person)
	{
		//overlap check
		bool bOverlap = false;
		for (unsigned int i = 0; i < mPassengersCnter; ++i)
		{
			if (this->GetPassenger(i) == person)
			{
				bOverlap = true;
			}
		}


		if (mMaxPassengersCount <= mPassengersCnter || person == nullptr || bOverlap == true)
		{
			return false;
		}

		mSeats[mPassengersCnter] = person;
		++mPassengersCnter;
		return true;

	}

	bool Vehicle::RemovePassenger(unsigned int i)
	{
		if (1 > mPassengersCnter || mPassengersCnter - 1 < i)
		{
			return false;
		}
		else
		{
			delete mSeats[i];
			mSeats[i] = nullptr;

			for (unsigned int index = i; index < mPassengersCnter - 1; ++index)
			{
				mSeats[index] = mSeats[index + 1];
			}
			for (unsigned int index = mPassengersCnter - 1; index < mMaxPassengersCount; ++index)
			{
				//delete mSeats[index];
				mSeats[index] = nullptr;
			}

			--mPassengersCnter;
			return true;
		}
	}

	unsigned int Vehicle::GetPassengersCount() const
	{
		return mPassengersCnter;
	}

	unsigned int Vehicle::GetMaxPassengersCount() const
	{
		return mMaxPassengersCount;
	}

	const Person** Vehicle::GetPassengers() const
	{
		if (mMaxPassengersCount == 0)
		{
			return nullptr;
		}
		else
		{
			return mSeats;
		}
	}

	const Person* Vehicle::GetPassenger(unsigned int i) const
	{
		if (mPassengersCnter - 1 < i)
		{
			return nullptr;
		}
		else
		{
			return mSeats[i];
		}
	}

	unsigned int Vehicle::GetPassengersTotalWeight() const
	{
		/*if (this->GetPassengersCount() == 0)
		{
			return 0;
		}*/

		unsigned int totalWeight = 0;

		for (unsigned int i = 0; i < mPassengersCnter; ++i)
		{
			totalWeight = totalWeight + mSeats[i]->GetWeight();
		}

		return totalWeight;
	}

	void Vehicle::SetPassengersZero()
	{
		mPassengersCnter = 0;
	}

	void Vehicle::IncreaseTravelCount()
	{
		/*if (mTravelCnt == mTravelLimit)
		{
			if (mRestCnt == mRestLimit)
			{
				mTravelCnt = 0;
				mRestCnt = 0;
				mbRestCheck = false;
				return;
			}

			mbRestCheck = true;
			++mRestCnt;
		}

		if (mbRestCheck != true)
		{
			++mTravelCnt;
			mTotalTravelCnt += mTravelCnt;
		}*/

		if (mbTravelCheck == true)
		{
			if (mTravelCnt == mTravelLimit)
			{
				mbTravelCheck = false;
				mbRestCheck = true;
				mRestCnt = 0;
				++mRestCnt;
				return;
			}

			++mTravelCnt;
			++mTotalTravelCnt;

		}
		else if (mbRestCheck == true)
		{
			if (mRestCnt == mRestLimit)
			{
				mbRestCheck = false;
				mbTravelCheck = true;
				mTravelCnt = 0;
				++mTravelCnt;
				++mTotalTravelCnt;
				return;
			}

			++mRestCnt;

		}

	}

	unsigned int Vehicle::GetTravelDistance()
	{
		unsigned int totalTravelCnt = this->GetTotalTravelCnt();
		unsigned int maxSpeed = this->GetMaxSpeed();
		return totalTravelCnt * maxSpeed;
	}

	unsigned int Vehicle::GetTotalTravelCnt() const
	{
		return mTotalTravelCnt;
	}

	unsigned int Vehicle::GetTravelCnt() const
	{
		return mTravelCnt;
	}

	unsigned int Vehicle::GetTravelLimit() const
	{
		return mTravelLimit;
	}

	unsigned int Vehicle::GetRestCnt() const
	{
		return mRestCnt;
	}

	unsigned int Vehicle::GetRestLimit() const
	{
		return mRestLimit;
	}
}