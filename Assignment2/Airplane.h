#pragma once

#include "Boatplane.h"
#include "IFlyable.h"
#include "IDrivable.h"
#include "Vehicle.h"

namespace assignment2
{
	class Boat;

	class Airplane : public Vehicle, public IFlyable, public IDrivable
	{
	public:
		Airplane(unsigned int maxPassengersCount);
		~Airplane();
		//Airplane& operator=(const Airplane& rhs);

		unsigned int GetMaxSpeed()const;
		unsigned int GetFlySpeed() const;
		unsigned int GetDriveSpeed() const;

		/*unsigned int GetTravelDistance() const;
		unsigned int GetVehicleTravelCount() const;
		*/

		Boatplane operator+(Boat& boat);
	
	private:
		
	};
}