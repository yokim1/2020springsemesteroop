#include "Airplane.h"
#include "Boat.h"

namespace assignment2
{
	Airplane::Airplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		this->mTravelLimit = 1;
		this->mRestLimit = 3;
	}

	Airplane::~Airplane()
	{
	}

	unsigned int Airplane::GetMaxSpeed() const
	{
		unsigned int flySpeed = this->GetFlySpeed();
		unsigned int driveSpeed = this->GetDriveSpeed();

		if (flySpeed > driveSpeed)
		{
			return flySpeed;
		}
		else
		{
			return driveSpeed;
		}

		return 0;
	}

	unsigned int Airplane::GetFlySpeed() const
	{
		unsigned int totalWeight = (this->GetPassengersTotalWeight());
		double result;
		result = 200.0 * exp((800.0 - totalWeight) / 500.0);

		return static_cast<unsigned int>(round(result));
	}

	unsigned int Airplane::GetDriveSpeed() const
	{
		unsigned int totalWeight = (this->GetPassengersTotalWeight());
		double result;
		result = 4.0 * exp((400.0 - totalWeight) / 70.0);

		return static_cast<unsigned int>(round(result));
	}

	Boatplane Airplane::operator+(Boat& boat)
	{
		
		unsigned int planePassengersNumber = this->GetPassengersCount();
		unsigned int boatPassengersNumber = boat.GetPassengersCount();
		unsigned int totalMaxPassengers = this->GetMaxPassengersCount() + boat.GetMaxPassengersCount();
		
		this->SetPassengersZero();
		boat.SetPassengersZero();

		return Boatplane(totalMaxPassengers, planePassengersNumber, boatPassengersNumber, this->GetPassengers(), boat.GetPassengers());
	}

	/*unsigned int Airplane::GetVehicleTravelCount() const
	{
		
	}

	unsigned int Airplane::GetTravelDistance() const
	{
		
	}

	bool Airplane::Traveling() const
	{
		
	}*/
}