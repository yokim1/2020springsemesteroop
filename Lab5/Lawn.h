#pragma once

#include "eGrassType.h"
#include <math.h>

#define PI 3.14

namespace lab5
{
	class Lawn
	{
	public:
		Lawn();
		Lawn(double);
		Lawn(double, double);
		virtual ~Lawn();

		virtual unsigned int GetArea() const = 0;

		unsigned int GetGrassPrice(eGrassType grassType) const;
		unsigned int GetMinimumSodRollsCount() const;

		inline double GetWidth() const;
		inline double GetHieght() const;
		inline double GetLength() const;
		inline double GetCircumference() const;

		inline void SetAreaCir(double);
		inline void SetAreaRec(double, double);
		inline void SetAreaET(double);

	private:
		/*double mWidth;
		double mHieght;
		double mLength;
		double mArea;
		double mCircumference;
		double mPrice;*/

		double mWidth;
		double mHieght;
		double mLength;
		double mArea;
		double mCircumference;
		double mPrice;
	};

	double Lawn::GetWidth() const
	{
		return mWidth;
	}

	double Lawn::GetHieght() const
	{
		return mHieght;
	}

	double Lawn::GetLength() const
	{
		return mLength;
	}

	double Lawn::GetCircumference() const
	{
		return mCircumference;
	}




	void Lawn::SetAreaCir(double radius)
	{
		mCircumference = 2 * PI * radius;
		mArea = round(PI * radius * radius);
	}

	void Lawn::SetAreaRec(double width, double hieght)
	{
		mCircumference = width * 2 + hieght * 2;
		mArea = width * hieght;
	}

	void Lawn::SetAreaET(double length)
	{
		mCircumference = length * 3;
		mArea = length * length * 0.25 * sqrt(3);
	}
}

