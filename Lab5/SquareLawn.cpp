#include "SquareLawn.h"

namespace lab5
{
	SquareLawn::SquareLawn(double length)
		: RectangleLawn(length, length)
	{
		
	}

	unsigned int SquareLawn::GetArea() const
	{
		
		return static_cast<unsigned int>(GetWidth() * GetWidth());
	}

	unsigned int SquareLawn::GetMinimumFencesCount() const
	{
		return static_cast<unsigned int>(GetCircumference() / 0.25);
	}

	unsigned int SquareLawn::GetFencePrice(eFenceType material)const
	{
		double price = 0;

		switch (material)
		{
		case RED_CEDAR:
			price = 6;
			break;
		
		case SPRUCE:
			price = 7;
			break;

		default:
			return 0;
		}

		return static_cast<unsigned int>((GetWidth() * 2 + GetHieght() * 2) * price);
	}
}