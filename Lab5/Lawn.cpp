#include "Lawn.h"

namespace lab5
{
	Lawn::Lawn()
	{
	}

	Lawn::Lawn(double length)
		: mLength(length)
	{

	}
	
	Lawn::Lawn(double width, double hieght)
		: mWidth(width), mHieght(hieght)
	{

	}

	Lawn::~Lawn()
	{
	}

	unsigned int Lawn::GetGrassPrice(eGrassType grassType) const
	{
		

		switch (grassType)
		{
			

		case BERMUDA:
			return static_cast<unsigned int>(round(mArea * 8));
			break;

		case BAHIA:
			return static_cast<unsigned int>(round(mArea * 5));
			break;

		case BENTGRASS:
			return static_cast<unsigned int>(round(mArea * 3));
			break;

		case PERENNIAL_RYEGRASS:
		{
			return static_cast<unsigned int>(round(mArea * 2.5));
			break;
		}
		case ST_AUGUSTINE:
			return static_cast<unsigned int>(round(mArea * 4.5));
			break;

		default: 
			return 0;
		}
		return 0;
	}

	unsigned int Lawn::GetMinimumSodRollsCount() const
	{
		double res = static_cast<unsigned int>(mArea) / 0.3;
		unsigned int uiRes = static_cast<unsigned int>(res);

		if (res == static_cast<double>(uiRes))
		{
			return uiRes;
		}
		else
		{
			return uiRes + 1;
		}
	}
}