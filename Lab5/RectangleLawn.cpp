#include "RectangleLawn.h"

namespace lab5 
{
	RectangleLawn::RectangleLawn(double width, double hieght)
		: Lawn(width, hieght)
	{
		SetAreaRec(width, hieght);
	}

	unsigned int RectangleLawn::GetArea() const
	{
		return static_cast<unsigned int>(GetWidth() * GetHieght());
	}

	unsigned int RectangleLawn::GetMinimumFencesCount() const
	{
		return static_cast<unsigned int>(GetCircumference() / 0.25);
	}

	unsigned int RectangleLawn::GetFencePrice(eFenceType material) const
	{
		double price = 0; 

		switch (material)
		{
		case RED_CEDAR:
			price = 6;
			break;
		case SPRUCE:
			price = 7;
			break;
		default: 
			return 0;
		}

		return static_cast<unsigned int>((GetWidth() * 2 + GetHieght() * 2) * price);
	}
}