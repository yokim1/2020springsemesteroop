#include <cassert>
#include <iostream>

#include "Lawn.h"
#include "IFenceable.h"
#include "RectangleLawn.h"
#include "CircleLawn.h"
#include "SquareLawn.h"
#include "EquilateralTriangleLawn.h"

using namespace lab5;
using namespace std;

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	unsigned int area, grassPrice, sodRollsCount;
	unsigned int fencePrice, fencesCount;

	Lawn* lawn = new SquareLawn(5);
	SquareLawn* squareLawn = (SquareLawn*)lawn;
	area = lawn->GetArea();
	grassPrice = lawn->GetGrassPrice(BENTGRASS);
	sodRollsCount = lawn->GetMinimumSodRollsCount();
	fencePrice = squareLawn->GetFencePrice(SPRUCE);
	fencesCount = squareLawn->GetMinimumFencesCount();
	assert(area == 25);
	assert(grassPrice == 75);
	assert(sodRollsCount == 84);
	assert(fencePrice == 140);
	assert(fencesCount == 80);
	squareLawn = nullptr;
	delete lawn;
	cout << "SquareLawn Clear!" << endl;

	lawn = new CircleLawn(2);
	area = lawn->GetArea();
	grassPrice = lawn->GetGrassPrice(PERENNIAL_RYEGRASS);
	sodRollsCount = lawn->GetMinimumSodRollsCount();
	assert(area == 13);
	assert(grassPrice == 33);
	assert(sodRollsCount == 44);
	delete lawn;

	lawn = new CircleLawn(6);
	area = lawn->GetArea();
	grassPrice = lawn->GetGrassPrice(ST_AUGUSTINE);
	sodRollsCount = lawn->GetMinimumSodRollsCount();
	assert(area == 113);
	assert(grassPrice == 509);
	assert(sodRollsCount == 377);
	delete lawn;
	cout << "CircleLawn Clear!" << endl;


	system("pause");
	return 0;
}