#include "EquilateralTriangleLawn.h"

namespace lab5
{
	EquilateralTriangleLawn::EquilateralTriangleLawn(double side)
		: Lawn(side)
	{
		SetAreaET(side);
	}

	
	unsigned int EquilateralTriangleLawn::GetArea() const
	{
		return static_cast<unsigned int>(GetLength() * GetLength() * 0.25 * sqrt(3.0));
	}

	unsigned int EquilateralTriangleLawn::GetMinimumFencesCount() const
	{
		return static_cast<unsigned int>(GetCircumference() / 0.25);
	}

	unsigned int EquilateralTriangleLawn::GetFencePrice(eFenceType material)const
	{
		double price = 0;

		switch (material)
		{
		case RED_CEDAR:
			price = 6;
			break;
		case SPRUCE:
			price = 7;
			break;
		default: 
			return 0;
		}
		
		return static_cast<unsigned int>((GetLength() * 3) * price);
	}
}