#include "CircleLawn.h"

namespace lab5
{
	CircleLawn::CircleLawn(double radius)
		: Lawn(radius)
	{
		SetAreaCir(radius);
	}

	unsigned int CircleLawn::GetArea() const
	{
		return static_cast<unsigned int>(round(GetLength() * GetLength() * PI));
	}
}