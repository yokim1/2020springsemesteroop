#pragma once
//#include "Lawn.h"
#include "RectangleLawn.h"
#include "IFenceable.h"

namespace lab5 
{
	class SquareLawn : public RectangleLawn
	{
	public:
		SquareLawn(double);

		unsigned int GetArea() const;

		unsigned int GetMinimumFencesCount() const;
		unsigned int GetFencePrice(eFenceType)const;
	};
}
